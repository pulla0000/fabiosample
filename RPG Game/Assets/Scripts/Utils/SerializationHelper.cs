﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using UnityEngine;

namespace MagicianGames
{
    public static class SerializationHelper
    {
        private static System.Type[] extras;
        #region Json

        public static string SerializeToJson<T>(T obj, bool encrypt = false, string encryptKey = Encryption.ENCRYPTATION_KEY)
        {
            string json = JsonUtility.ToJson(obj);

            if (encrypt)
                json = Encryption.Encrypt(json, encryptKey);

            return json;
        }

        public static T DeserializeFromJson<T>(string json, bool encrypted = false, string encryptKey = Encryption.ENCRYPTATION_KEY)
        {
            if (encrypted)
                json = Encryption.Decrypt(json, encryptKey);

            T obj = (T)JsonUtility.FromJson<T>(json);
            return obj;
        }

        #endregion

        #region XML

        public static string SerializeToXML<T>(T obj, bool encrypt = false, string encryptKey = Encryption.ENCRYPTATION_KEY)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(T), extras);
            StringWriter writer = new StringWriter();

            serializer.Serialize(writer, obj);
            string xmlText = writer.ToString();
            writer.Close();

            if (encrypt)
                xmlText = Encryption.Encrypt(xmlText, encryptKey);

            return xmlText;
        }

        public static T DeserializeFromXml<T>(string xmlText, bool encrypted = false, string encryptKey = Encryption.ENCRYPTATION_KEY)
        {
            if (encrypted)
                xmlText = Encryption.Decrypt(xmlText, encryptKey);

            XmlSerializer serializer = new XmlSerializer(typeof(T), extras);
            StringReader reader = new StringReader(xmlText);
            T obj = (T)serializer.Deserialize(reader);
            reader.Close();
            return obj;
        }

        #endregion
    }
}