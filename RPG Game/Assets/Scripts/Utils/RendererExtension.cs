﻿using UnityEngine;

namespace MagicianGames
{
    public static class RendererExtension
    {
        public static void SetTexture(this Renderer renderer, string textureName, Texture texture,
            MaterialPropertyBlock block = null)
        {
            if (block == null)
            {
                block = new MaterialPropertyBlock();
            }
            
            renderer.GetPropertyBlock(block);
            block.SetTexture(textureName, texture);
            renderer.SetPropertyBlock(block);
        }
        
        public static void SetColor(this Renderer renderer, Color color, string colorName = "_Color",
            MaterialPropertyBlock block = null)
        {
            if (block == null)
            {
                block = new MaterialPropertyBlock();
            }
            
            renderer.GetPropertyBlock(block);
            
            block.SetColor(colorName, color);
            
            renderer.SetPropertyBlock(block);
        }
    }
}