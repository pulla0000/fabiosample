﻿using UnityEngine;
using System.IO;
using System.Threading.Tasks;
using Core.Environment;

namespace MagicianGames
{
    public class FileHelper
    {
        private const string FILE_EXTENSION = ".fg";

        private static string GetFileDirectory()
        {
            if (EnvironmentVariables.IsEditor)
            {
                return Application.persistentDataPath + "/Editor/";
            }
                
            return Application.persistentDataPath+"/";
        }
        
        #region Save

        public static bool HasFile(string fileName)
        {
            var filePath = GetFileDirectory() + fileName + FILE_EXTENSION;
            return File.Exists(filePath);
        }

        /// <summary>
        /// Save a string data to a file
        /// </summary>
        /// <param name="data">Data to be writen in the file</param>
        /// <param name="fileName">File name without extension</param>
        /// <param name="encrypt">If you want to encrypt this data</param>
        public static void SaveData(string data, string fileName, bool encrypt = true)
        {
            var dir = GetFileDirectory();
            var filePath = dir + fileName + FILE_EXTENSION;

#if UNITY_EDITOR
            Debug.Log("#SAVING SYSTEM# Saving file at: " + filePath);
#endif
            Task.Run(() =>
            {
                if (!Directory.Exists(dir))
                    Directory.CreateDirectory(dir);

                if (encrypt)
                    data = Encryption.Encrypt(data);

                StreamWriter writer = new StreamWriter(filePath);
                writer.Write(data);
                writer.Close();
                writer.Dispose();
            });
        }

        /// <summary>
        /// Save an Object to a file in XML Format
        /// </summary>
        /// <typeparam name="T">The object Tipe</typeparam>
        /// <param name="objToSave">Object to save</param>
        /// <param name="fileName">File name without extension</param>
        /// <param name="encrypt">If you want to encrypt this data</param>
        public static void SaveToXmlFile<T>(T objToSave, string fileName, bool encrypt = true)
        {
            var data = SerializationHelper.SerializeToXML(objToSave, false);
            SaveData(data, fileName, encrypt);
        }

        /// <summary>
        /// Save an Object to a file in JSON Format
        /// </summary>
        /// <typeparam name="T">The object Tipe</typeparam>
        /// <param name="objToSave">Object to save</param>
        /// <param name="fileName">File name without extension</param>
        /// <param name="encrypt">If you want to encrypt this data</param>
        public static void SaveToJsonFile<T>(T objToSave, string fileName, bool encrypt = true)
        {
            var data = SerializationHelper.SerializeToJson(objToSave, false);
            SaveData(data, fileName, encrypt);
        }

        #endregion

        #region Load

        /// <summary>
        /// Load a saved file
        /// </summary>
        /// <param name="fileName">File name without extension</param>
        /// <param name="isEncrypted">If the data you want to load is encrypted</param>
        /// <returns>The file data string</returns>
        public static async Task<string> LoadDataAsync(string fileName, bool isEncrypted = true)
        {
            var filePath =GetFileDirectory() + fileName + FILE_EXTENSION;
            string data = null;

#if UNITY_EDITOR
            Debug.Log("#SAVING SYSTEM# Loading file from: " + filePath);
#endif

            await Task.Run(() =>
              {
                  if (File.Exists(filePath))
                  {
                      data = File.ReadAllText(filePath);

                      if (isEncrypted)
                          data = Encryption.Decrypt(data);
                  }
              });

            return data;
        }

        /// <summary>
        /// Load a saved file
        /// </summary>
        /// <param name="fileName">File name without extension</param>
        /// <param name="isEncrypted">If the data you want to load is encrypted</param>
        /// <returns>The file data string</returns>
        public static string LoadData(string fileName, bool isEncrypted = true)
        {
            var filePath = Application.persistentDataPath + "/" + fileName + FILE_EXTENSION;
            string data = null;

#if UNITY_EDITOR
            Debug.Log("#SAVING SYSTEM# Loading file from: " + filePath);
#endif

            if (File.Exists(filePath))
            {
                data = File.ReadAllText(filePath);

                if (isEncrypted)
                    data = Encryption.Decrypt(data);
            }

            return data;
        }
        
        /// <summary>
        /// Load a saved data from a XML file
        /// </summary>
        /// <param name="fileName">File name without extension</param>
        /// <param name="isEncrypted">If the data you want to load is encrypted</param>
        /// <returns>The Object</returns>
        public static async Task<T> LoadFromXmlFileAsync<T>(string fileName, bool isEncrypted = true)
        {
            var data = await LoadDataAsync(fileName, isEncrypted);

            if (data == null)
                return default;

            return SerializationHelper.DeserializeFromXml<T>(data, false);
        }

        /// <summary>
        /// Load a saved data from a XML file
        /// </summary>
        /// <param name="fileName">File name without extension</param>
        /// <param name="isEncrypted">If the data you want to load is encrypted</param>
        /// <returns>The Object</returns>
        public static T LoadFromXmlFile<T>(string fileName, bool isEncrypted = true)
        {
            var data = LoadData(fileName, isEncrypted);

            if (data == null)
                return default;

            return SerializationHelper.DeserializeFromXml<T>(data, false);
        }
        
        /// <summary>
        /// Load a saved data from a JSON file
        /// </summary>
        /// <param name="fileName">File name without extension</param>
        /// <param name="isEncrypted">If the data you want to load is encrypted</param>
        /// <returns>The Object</returns>
        public static async Task<T> LoadFromJsonFileAsync<T>(string fileName, bool isEncrypted = true)
        {
            var data = await LoadDataAsync(fileName, isEncrypted);

            if (data == null)
            {
                return default;
            }
            return SerializationHelper.DeserializeFromJson<T>(data, false);
        }

        /// <summary>
        /// Load a saved data from a JSON file
        /// </summary>
        /// <param name="fileName">File name without extension</param>
        /// <param name="isEncrypted">If the data you want to load is encrypted</param>
        /// <returns>The Object</returns>
        public static T LoadFromJsonFile<T>(string fileName, bool isEncrypted = true)
        {
            var data =  LoadData(fileName, isEncrypted);

            if (data == null)
            {
                return default;
            }
            return SerializationHelper.DeserializeFromJson<T>(data, false);
        }
        
        #endregion
    }
}