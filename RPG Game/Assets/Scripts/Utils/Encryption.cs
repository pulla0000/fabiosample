﻿using System.Security.Cryptography;
using System.Text;

namespace MagicianGames
{
    public static class Encryption
    {
        public const string ENCRYPTATION_KEY = "cbfJ3&N=-_4&uW5TS3=9wp5SU93puW=u";// "funkidstecnologiaTest@1234Fabeta";

        #region Rijndael

        public static string Encrypt(string toEncrypt, string encryptKey = ENCRYPTATION_KEY)
        {
            try
            {
                byte[] keyArray = UTF8Encoding.UTF8.GetBytes(encryptKey);
                // 256-AES key
                byte[] toEncryptArray = UTF8Encoding.UTF8.GetBytes(toEncrypt);
                RijndaelManaged rDel = new RijndaelManaged();
                rDel.Key = keyArray;
                rDel.Mode = CipherMode.ECB;
                // http://msdn.microsoft.com/en-us/library/system.security.cryptography.ciphermode.aspx
                rDel.Padding = PaddingMode.PKCS7;

                // better lang support
                ICryptoTransform cTransform = rDel.CreateEncryptor();
                byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);
                return System.Convert.ToBase64String(resultArray, 0, resultArray.Length);
            }
            catch
            {
                return null;
            }
        }

        public static string Decrypt(string toDecrypt, string encryptKey = ENCRYPTATION_KEY)
        {
            try
            {
                byte[] keyArray = UTF8Encoding.UTF8.GetBytes(encryptKey);
                // AES-256 key
                byte[] toEncryptArray = System.Convert.FromBase64String(toDecrypt);
                RijndaelManaged rDel = new RijndaelManaged();
                rDel.Key = keyArray;
                rDel.Mode = CipherMode.ECB;
                // http://msdn.microsoft.com/en-us/library/system.security.cryptography.ciphermode.aspx
                rDel.Padding = PaddingMode.PKCS7;
                // better lang support
                ICryptoTransform cTransform = rDel.CreateDecryptor();
                byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);
                return UTF8Encoding.UTF8.GetString(resultArray);
            }
            catch
            {
                return null;
            }
        }

        #endregion
    }
}