﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MagicianGames
{
    public class ScreenName
    {
        public const string PLAY_SCREEN = "Play Screen", OPTIONS_SCREEN = "Options Screen", CREDITS_SCREEN = "Credits Screen";
    }
}