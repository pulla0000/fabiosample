﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace MagicianGames.Editor.Helpers
{
    public class SavedFileHelper 
    {
        [MenuItem("Magician Games/Open Files Folder")]
        public static void OpenFilesFolder()
        {
            EditorUtility.RevealInFinder(Application.persistentDataPath);
        }
    }
}
