﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MagicianGames.Intro.Initializer
{
    public class GameInitializer : IGameInitializer
    {
        public event Action OnGameInitialized;

        public void Initialize()
        {
            OnGameInitialized?.Invoke();
        }
    }
}