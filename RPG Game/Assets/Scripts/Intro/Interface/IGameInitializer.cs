﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MagicianGames.Intro
{
    public interface IGameInitializer
    {
        event System.Action OnGameInitialized;
        void Initialize();
    }
}