﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using DG.Tweening;
using Debug = UnityEngine.Debug;

namespace MagicianGames.Intro
{
    public class IntroScreenController : MonoBehaviour
    {
        [SerializeField]
        private UnityEngine.UI.Image _logo;

        [Zenject.Inject]
        private IGameInitializer _gameInitializer;

        [Zenject.Inject]
        private IScenesLoader _scenesLoader;

        private bool _isGameInitialized;

        // Start is called before the first frame update
        void Start()
        {
            PlayFadeInAnimation();
            _gameInitializer.OnGameInitialized += OnGameInitialized;
            _gameInitializer.Initialize();
        }

        #region Fade In

        private void PlayFadeInAnimation()
        {
            _logo.DOFade(0, 0);
            _logo.transform.localScale = Vector3.one * .7f;

            _logo.DOFade(1, 1).SetEase(Ease.Linear).OnComplete(OnFadeInComplete);
            _logo.transform.DOScale(1, 1).SetEase(Ease.OutCubic);
        }

        public void OnFadeInComplete()
        {
            StartCoroutine(WaitGameInitialization());
        }

        #endregion

        #region Fade Out

        private void PlayFadeOutAnimation()
        {
            _logo.DOFade(0, .7f).SetDelay(1).SetEase(Ease.Linear).OnComplete(OnFadeOutComplete);
        }

        public void OnFadeOutComplete()
        {
            _scenesLoader?.AllowSceneActivation(false);
        }

        #endregion

        private IEnumerator WaitGameInitialization()
        {
            while (!_isGameInitialized)
                yield return 0;

            _scenesLoader.LoadScene("Title Screen", false, false);
            PlayFadeOutAnimation();
        }

        public void OnGameInitialized()
        {
            _isGameInitialized = true;
        }
    }
}