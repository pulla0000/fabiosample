using UnityEngine;
using Zenject;

namespace MagicianGames.Intro.Initializer
{
    public class GameIntroInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            Container.Bind<IGameInitializer>().To<GameInitializer>().AsCached();
        }
    } }