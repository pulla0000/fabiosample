﻿using MagicianGames.Core.ScreenNavigation;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace MagicianGames.Installer
{
    public class UiInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            Container.Bind<IScreenNavigation>().To<ScreenNavigation>().AsSingle();
        }
    }
}