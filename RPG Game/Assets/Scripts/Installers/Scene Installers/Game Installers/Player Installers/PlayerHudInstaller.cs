﻿using MagicianGames.AdventureGame.Game.UI;
using UnityEngine;
using Zenject;

namespace MagicianGames.AdventureGame.Game.Installer
{
    public class PlayerHudInstaller : MonoInstaller
    {
        [SerializeField]
        private GameObject _hudPrefab;

        public override void InstallBindings()
        {
            Container.InstantiatePrefab(_hudPrefab);
        }
    }
}