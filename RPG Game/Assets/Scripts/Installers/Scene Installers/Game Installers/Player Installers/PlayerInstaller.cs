using UnityEngine;
using Zenject;
using System.Linq;
using MagicianGames.AdventureGame.Game.Controllers;
using MagicianGames.AdventureGame.Game.Characters.Playable;
using MagicianGames.AdventureGame.Game.Health;
using MagicianGames.AdventureGame.Game.Skills;

namespace MagicianGames.AdventureGame.Game.Installer
{
    public class PlayerInstaller : MonoInstaller
    {
        [SerializeField]
        private Transform _playerTransform;//, _playerSpawnPoint;

        [SerializeField]
        private ProjectileBehaviour _playerProjectilePrefab;
        
        public override void InstallBindings()
        {
            //TODO: GET PLAYER HEALTH DATA

            var t = Instantiate(_playerTransform, Vector3.zero, Quaternion.identity);
            var spawnPoint = t.GetComponentsInChildren<ObjectSpawnPoint>().FirstOrDefault(o => o.Id == "Projectile").transform;

            var player = new PlayableCharacter(t, spawnPoint, _playerProjectilePrefab);
            Container.Bind<IPlayableCharacter>().FromInstance(player).AsCached();

            var playerHealth = t.GetComponent<CharacterHealth>();
            playerHealth.Setup(100);
            Container.Bind<IHealth>().WithId("Player").FromInstance(playerHealth).AsSingle();
            Container.Bind<IKillable>().WithId("Player").FromInstance(playerHealth).AsSingle();

            //Container.BindInterfacesTo<PlayerController>().AsSingle();

            var controller = new PlayerController();
            Container.BindInstance<IPlayerController>(controller).AsSingle();
            Container.BindInstance<ITickable>(controller).AsSingle();
            Container.QueueForInject(controller);

            playerHealth.OnDie += player.Die;
            playerHealth.OnDie += controller.Disable;
        }
    }
}