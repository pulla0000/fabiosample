﻿using UnityEngine;
using Zenject;

namespace MagicianGames.AdventureGame.Game.Input
{
    public class InputInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            Container.Bind<ICharacterInput>().To<CharacterInput>().AsSingle();
        }
    }
}