using Game.Camera;
using Game.Camera.Interfaces;
using UnityEngine;
using Zenject;

public class GameCameraInstaller : MonoInstaller<GameCameraInstaller>
{
    [SerializeField]
    private GameObject _cameraPrefab;
    
    public override void InstallBindings()
    {
        Container.Bind<IGameCamera>().FromComponentOn(_cameraPrefab).AsSingle();
    }
}