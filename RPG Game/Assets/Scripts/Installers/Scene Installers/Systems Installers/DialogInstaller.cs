﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;
using Zenject;

namespace MagicianGames.Installer
{
    public class DialogInstaller : MonoInstaller
    {
        [SerializeField]
        private AssetReference _dialogScreenPrefab;

        public override void InstallBindings()
        {
            var dialog = new Core.DialogSystem.DialogScreenController(_dialogScreenPrefab);
            Container.BindInstance<IDialogScreen>(dialog).AsSingle();
            Container.QueueForInject(dialog);

            //Container.Bind<IDialog>().FromInstance(new Core.DialogSystem.DialogScreenController(_dialogScreenPrefab)).AsSingle();
        }
    }
}