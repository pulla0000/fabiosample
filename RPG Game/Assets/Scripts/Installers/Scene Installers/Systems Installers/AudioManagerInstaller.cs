﻿using UnityEngine;
using Zenject;

namespace MagicianGames.Installer
{
    public class AudioManagerInstaller : MonoInstaller
    {
        [SerializeField]
        private GameObject _sfx, _uiSfx;

        public override void InstallBindings()
        {
            Container.Bind<ISfxManager>().WithId("Sfx").FromComponentOn(_sfx).AsCached();
            Container.Bind<ISfxManager>().WithId("UI Sfx").FromComponentOn(_uiSfx).AsCached();
        }
    }
}