using MagicianGames.AdventureGame;
using MagicianGames.Core.QuestSystem;
using UnityEngine;
using UnityEngine.AddressableAssets;
using Zenject;

namespace MagicianGames.Installer
{
    public class QuestsInstaller : MonoInstaller
    {
        [SerializeField]
        private QuestManager _questManagerPrefab;

        [SerializeField]
        private AssetReference _questViewReference;

        public override void InstallBindings()
        {
            Container.Bind<IQuestScreen>().FromInstance(new QuestScreenController(_questViewReference)).AsSingle();

            var questManager = Instantiate(_questManagerPrefab);
            Container.BindInstance<IQuest>(questManager).AsSingle();
            Container.QueueForInject(questManager);
        }
    }
}