using MagicianGames;
using MagicianGames.Core.AudioSystem;
using UnityEngine;
using UnityEngine.Audio;
using Zenject;

public class MasterAudioInstaller : MonoInstaller
{
    [SerializeField]
    private AudioMixer _masterAdioMixer;
    
    public override void InstallBindings()
    {
        Container.Bind<IMasterAudioSystem>().FromInstance(new MasterAudioSystem(_masterAdioMixer)).AsSingle().NonLazy();
    }
}