using System;
using Core.Input_System.Data;
using Core.InputSystem;
using UnityEngine;
using UnityEngine.InputSystem;
using Zenject;
using InputSystem = Core.InputSystem.InputSystem;

public class InputInstaller : MonoInstaller
{
    [SerializeField]
    private InputActionAsset _actionMap;

    [SerializeField]
    private InputSpriteData _spriteData;
    
    public override void InstallBindings()
    {
        var input = new InputSystem(_actionMap);
        Container.Bind<IInputSystem>().FromInstance(input ).AsSingle();
        Container.Bind<IDisposable>().FromInstance(input);

        Container.Bind<InputSpriteData>().FromInstance(_spriteData).AsSingle();
        Container.Bind<IDisposable>().FromInstance(_spriteData);
    }
}