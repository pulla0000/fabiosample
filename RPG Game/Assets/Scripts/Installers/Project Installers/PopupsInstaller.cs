using UnityEngine;
using Zenject;
using UnityEngine.AddressableAssets;
using MagicianGames.AdventureGame;

namespace MagicianGames.Installer
{
    public class PopupsInstaller : MonoInstaller
    {
        [SerializeField]
        private AssetReference _oneButtonPopupPrefab, _confirmationPopupPrefab, _fadeScreen;

        public override void InstallBindings()
        {
            Container.Bind<IOneButtonPopup>().FromInstance(new Core.Popup.OneButtonPopup.OneButtonPopupController(_oneButtonPopupPrefab)).AsSingle();
            Container.Bind<IConfirmationPopup>().FromInstance(new Core.Popup.TwoButtonsPopup.ConfirmationsPopupController(_confirmationPopupPrefab)).AsSingle();
            Container.Bind<IFadeScreen>().FromInstance(new MagicianGames.AdventureGame.Game.UI.FadeScreen.FadeScreenController(_fadeScreen)).AsSingle();
        }
    }
}