using UnityEngine;
using Zenject;
using UnityEngine.AddressableAssets;
using MagicianGames.AdventureGame.Game;
using MagicianGames.AdventureGame.Game.Systems.MapSystem;

namespace MagicianGames.Installer
{
    public class CoreInstaller : MonoInstaller
    {
        [SerializeField]
        private AssetReference _loadScreenPrefab;

        public override void InstallBindings()
        {
            //Cursor.visible = false;

            Container.Bind<IScenesLoader>().FromInstance(new Core.LoadingScreen.ScenesLoaderController(_loadScreenPrefab)).AsSingle();
            Container.Bind<IMapManager>().To<MapManager>().AsSingle();
        }
    }
}