﻿using MagicianGames.Core.SavingSystem;
using Zenject;

namespace MagicianGames.Installer
{
    public class DataInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            var settings = new SettingsDataManager();
            Container.Bind<ISettingsData>().FromInstance(settings).AsSingle();
            Container.QueueForInject(settings);
            
            Container.Bind<IGameData>().To<GameDataManager>().AsSingle();
        }
    }
}