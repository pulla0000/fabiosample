﻿namespace Core.Environment
{
    public class EnvironmentVariables
    {
        public static bool EncryptData()
        {
            return false;
            return !IsEditor;
        }

        public static bool IsEditor
        {
            get
            {
#if UNITY_EDITOR
                return true;
#endif
                return false;
            }
        }
}
}