﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MagicianGames.Core.QuestSystem
{
    [CreateAssetMenu(fileName = "New Kill Enemy Class Quest", menuName = "Magician Games/Quest/Create Kill Enemy Class")]
    public class KillEnemyClassQuest : BaseQuest
    {

        public override void Init()
        {

        }

        public override void Dispose()
        {
           
        }

    }
}