﻿using MagicianGames.AdventureGame;
using UnityEngine;

namespace MagicianGames.Core.QuestSystem
{
    [CreateAssetMenu(fileName = "New Kill Enemy Quest", menuName = "Magician Games/Quest/Create Kill Enemy")]
    [System.Serializable]
    public class KillEnemyQuest : BaseQuest
    {
        [SerializeField]
        private string _enemyId;

        public override void Init()
        {
            base.Init();
            NotificationsManager.AddListener<EnemyKilledNotification>(OnEnemyKilled);
        }

        public override void Dispose()
        {
            base.Dispose();
            NotificationsManager.RemoveListener<EnemyKilledNotification>(OnEnemyKilled);
        }

        private void OnEnemyKilled(EnemyKilledNotification e)
        {
            Debug.Log("KillEnemyQuest, got Killed event, my id: " + _enemyId + " killed: " + e.EnemyId);
            if (State != QuestState.Started)
                return;

            if (e.EnemyId == _enemyId)
            {
                Complete();
            }
        }
    }
}