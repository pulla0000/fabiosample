﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MagicianGames.Core.QuestSystem
{
    [CreateAssetMenu(fileName = "New Collect Item Quest", menuName = "Magician Games/Quest/Create Collect Item")]
    public class CollectItemQuest : BaseQuest
    {
        public override void Init()
        {

        }

        public override void Dispose()
        {

        }
    }
}