﻿using MagicianGames.Core.DialogSystem;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MagicianGames.Core.QuestSystem
{
    public enum QuestState
    {
        None= -1,
        Ready = 0,
        Started = 1,
        Completed = 2,
        Done = 3
    }

    [System.Serializable]
    public class BaseQuest : ScriptableObject
    {
        [SerializeField]
        private string _id, _title;

        [SerializeField]
        private DialogData _introDialog, _completeDialog, _acceptedDialog, _rejectedDialog;

        [SerializeField]
        private string /*_characterId, */_questText;

        [SerializeField]
        [HideInInspector]
        private QuestState _state;

        [SerializeField]
        private int _coinReward;

        public string Id => _id;
        public string Title => _title;
        public string Description => _questText;
        //public string CharacterId => _characterId;
        public QuestState State => _state;
        public int CoinReward => _coinReward;
        public DialogData IntroDialog => _introDialog;
        public DialogData CompletedDialog => _completeDialog;
        public DialogData AcceptedDialog => _acceptedDialog;
        public DialogData RejectedDialog => _rejectedDialog;

        public QuestSavableData Data { get; private set; }

        public event System.Action<string, QuestState> OnStateChange;

        public virtual void SetState(QuestState state)
        {
            _state = state;
            Data.SetData("State", _state);
            OnStateChange?.Invoke(_id, _state);
        }

        public virtual void Start()
        {
            SetState(QuestState.Started);
        }

        public virtual void Complete()
        {
            SetState(QuestState.Completed);
        }

        public virtual void SetData(QuestSavableData data)
        {
            Data = data;

            if (Data == null)
            {
                _state = QuestState.Ready;
                Data = new QuestSavableData() { Id = _id };
                Data.SetData("State", _state);
            }
            else
            {
                var state = Data.GetData("State");

                if (state != null)
                {
                    _state = (QuestState) state;
                }
            }
        }

        //public void SetDialog(DialogData dialog)
        //{
        //    _dialog = dialog;
        //}

        public virtual void Init()
        {
            //TODO REMOVE IT
            if (_state == QuestState.None)
                _state = QuestState.Ready;
        }

        public virtual void Dispose()
        {

        }
    }
}