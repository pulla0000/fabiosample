﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace MagicianGames.Core.QuestSystem
{
    [System.Serializable]
    public class QuestSavableData
    {
        public string Id;
        [SerializeField]
        public List<SavableData> Data;

        public QuestSavableData()
        {
            Data = new List<SavableData>();
        }

        public void SetData(string key, object data)
        {
            SavableData obj = Data.FirstOrDefault(k => k.Key == key);

            if (obj == null)
            {
                obj = new SavableData() { Key = key };
                obj.Data = data;
                Data.Add(obj);
            }
            else
                obj.Data = data;
        }

        public object GetData(string key)
        {
            var obj = Data.FirstOrDefault(k => k.Key == key);

            return obj?.Data;
        }

        public override string ToString()
        {
            string s = "Quest data: " + Id;

            foreach (var data in Data)
            {
                string val = "null";

                if (data.Data != null)
                    val = data.Data.ToString();

                s += "\n Key: " + data.Key + " Val: " + val;
            }
            
            return s;
        }

        [System.Serializable]
        public class SavableData
        {
            public string Key;
            public object Data;
        }
    }
}