﻿using MagicianGames.AdventureGame;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;

namespace MagicianGames.Core.QuestSystem
{
    public class QuestScreenController : IQuestScreen
    {
        private QuestScreenView _view;
        private AssetReference _defaultReference, _overrideReference;

        private BaseQuest _currentQuest;

        private System.Action _onClose;
        private UIInput _uiInput;
        
        public QuestScreenController(AssetReference defaultView)
        {
            _defaultReference = defaultView;
        }

        public bool IsOpen => _view == null ? false : _view.IsOpen;
        private AssetReference ViewReference => _overrideReference ?? _defaultReference;

        public void OverrideGraphics(AssetReference viewReference)
        {
            _overrideReference = viewReference;
        }

        public async void Show(BaseQuest quest, System.Action onClose)
        {
            if (IsOpen || quest.State != QuestState.Ready)
                return;

            if (_view == null)
            {
                _view = await AddressablesHelper.Instantiate<QuestScreenView>(ViewReference);
                _view.OnDecisionMade += OnDecisionMade;
            }

            _onClose = onClose;
            _currentQuest = quest;
            _view.Open(_currentQuest);
        }

        private void OnDecisionMade(bool accepted)
        {
            if (accepted)
                _currentQuest.Start();

            Close();
        }

        public void Close()
        {
            Debug.Log("#QUEST SCREEN# CLOSE()");
            _view?.Close();
            _onClose?.Invoke();
        }

        private void UnloadView()
        {
            if (_view != null)
                Addressables.ReleaseInstance(_view.gameObject);
        }
    }
}