﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.InputSystem;
using UnityEngine.UI;

namespace MagicianGames.Core.QuestSystem
{
    public class QuestScreenView : MonoBehaviour
    {
        [SerializeField]
        private GameObject _window;

        [SerializeField]
        private TextMeshProUGUI _titleText, _descriptionText, _rewardText;

        [SerializeField]
        private InputActionReference _acceptQuestAction;
        
        [SerializeField]
        private InputActionReference _rejectQuestAction;
        
        public event System.Action<bool> OnDecisionMade;
        public bool IsOpen => _window.activeSelf;

        // Start is called before the first frame update
        void Start()
        {
        }

        private void RegisterInput()
        {
            _acceptQuestAction.action.performed +=  OnAcceptClicked;
            _rejectQuestAction.action.performed +=  OnRejectClicked;
        }

        private void UnregisterInput()
        {
            _acceptQuestAction.action.performed -=  OnAcceptClicked;
            _rejectQuestAction.action.performed -=  OnRejectClicked;
        }
        
        private void EnableInput()
        {
            RegisterInput();
            _acceptQuestAction.action.Enable();
            _rejectQuestAction.action.Enable();
        }

        private void DisableInput()
        {
            UnregisterInput();
            _acceptQuestAction.action.Disable();
            _rejectQuestAction.action.Disable();
        }
        
        public void Open(BaseQuest quest)
        {
            _titleText.text = quest.Title;
            _descriptionText.text = quest.Description;
            _rewardText.text = "$" + quest.CoinReward;
            _window.SetActive(true);
            EnableInput();
        }

        public void Close()
        {
            DisableInput();
            _window.SetActive(false);
        }

        private void OnAcceptClicked(InputAction.CallbackContext ctx)
        {
            OnDecisionMade?.Invoke(true);
        }

        private void OnRejectClicked(InputAction.CallbackContext ctx)
        {
            OnDecisionMade?.Invoke(false);
        }
    }
}