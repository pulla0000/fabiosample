﻿using MagicianGames.AdventureGame;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace MagicianGames.Core.QuestSystem
{
    public class QuestManager : MonoBehaviour, IQuest
    {
        [SerializeField]
        private BaseQuest[] _allQuests;

        private IGameData _gameData;

        [Zenject.Inject]
        public void SetGameData(IGameData gameData)
        {
            _gameData = gameData;
            bool save = false;

            foreach (var quest in _allQuests)
            {
                quest.Init();
                quest.OnStateChange += OnQuestStateChanged;
                
                var data = _gameData.GameData.QuestData.GetData(quest.Id);
                quest.SetData(data);

                if (data == null)
                {
                    _gameData.GameData.QuestData.Quests.Add(quest.Data);
                    save = true;
                }
            }

            if (save)
                Save();
        }

        private void OnQuestStateChanged(string id, QuestState state)
        {
            Save();
        }

        public BaseQuest GetQuest(string id)
        {
            var quest = _allQuests.FirstOrDefault(q => q.Id == id);

            //if (quest == null)
            //{
            //    var original = _allQuests.FirstOrDefault(q => q.Id == id);

            //    if (original == null)
            //    {
            //        Debug.LogError("#QUEST# Quest with id " + id + " does not exist");
            //        return null;
            //    }

            //    quest = ScriptableObject.CreateInstance<BaseQuest>();
            //    quest.Clone(original);
            //    quest.Init();
            //    _gameData.GameData.QuestData.Quests.Add(quest);
            //}
            //Debug.Log("#QUEST# On GET Saved Quests: " + _gameData.GameData.QuestData.Quests.Count);
            //return quest;

            return quest;
        }

        public void Save()
        {
            _gameData.Save();
        }
    }
}
