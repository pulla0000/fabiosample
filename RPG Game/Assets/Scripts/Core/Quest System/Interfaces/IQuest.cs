﻿using MagicianGames.Core.QuestSystem;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MagicianGames.AdventureGame
{
    public interface IQuest
    {
        BaseQuest GetQuest(string id);
        void Save();
    }
}