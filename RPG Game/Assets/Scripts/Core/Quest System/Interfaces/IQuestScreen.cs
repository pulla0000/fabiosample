﻿using MagicianGames.Core.QuestSystem;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;

namespace MagicianGames.AdventureGame
{
    public interface IQuestScreen
    {
        bool IsOpen { get; }
        void OverrideGraphics(AssetReference viewReference);
        void Show(BaseQuest quest, System.Action onClose);
    }
}