﻿using System.Collections;
using UnityEngine;

namespace Core.RoutineRunner
{
    public class RoutineRunner : MonoBehaviour
    {
        private static RoutineRunner m_instance;
        
        private static void Init()
        {
            if (m_instance != null)
                return;

            m_instance = FindObjectOfType<RoutineRunner>();

            if (m_instance == null)
            {
                m_instance = new GameObject("Notifications Manager").AddComponent<RoutineRunner>();
            }

            DontDestroyOnLoad(m_instance.gameObject);
        }

        public static void CoroutineStart(IEnumerator coroutine)
        {
            Init();
            m_instance.StartCoroutine(coroutine);
        }
        
        public static void CoroutineStop(IEnumerator coroutine)
        {
            Init();
            m_instance.StopCoroutine(coroutine);
        }
    }
}