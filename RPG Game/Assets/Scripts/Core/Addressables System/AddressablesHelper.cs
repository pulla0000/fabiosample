﻿using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.AddressableAssets;

namespace MagicianGames
{
    public static class AddressablesHelper
    {
        public static async Task<T> Instantiate<T>(AssetReference asset) where T : Component
        {
            var result = await Addressables.InstantiateAsync(asset, Vector3.zero, Quaternion.identity).Task;
            return result.GetComponent<T>();
        }

        public static async Task<GameObject> Instantiate(AssetReference asset)
        {
            var result = await Addressables.InstantiateAsync(asset, Vector3.zero, Quaternion.identity).Task;
            return result;
        }

        public static async Task<T> LoadAsset<T>(AssetReference asset)
        {
            var result = await asset.LoadAssetAsync<T>().Task;
            return result;
        }
        
        public static void LoadAsset<T>(AssetReference asset, System.Action<T> callback)
        {
           asset.LoadAssetAsync<T>().Completed+=
               result =>
               {
                   callback?.Invoke(result.Result);
               };
        }
    }
}