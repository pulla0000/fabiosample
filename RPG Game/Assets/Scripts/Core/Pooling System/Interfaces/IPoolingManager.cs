﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MagicianGames
{
    public interface IPoolingManager
    {
        Transform GetInstance(Transform prefab, Vector3 position, Quaternion rotation, bool autoActivate = true);
    }
}