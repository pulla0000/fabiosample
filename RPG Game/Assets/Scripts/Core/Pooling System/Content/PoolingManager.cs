﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace MagicianGames.Core.PoolingSystem
{
    public class PoolingManager : IPoolingManager
    {
        private Dictionary<Transform, List<Transform>> _instances;

        public PoolingManager()
        {
            _instances = new Dictionary<Transform, List<Transform>>();
        }

        public Transform GetInstance(Transform prefab, Vector3 position, Quaternion rotation, bool autoActivate = true)
        {
            var instance = FindInstance(prefab);

            if (instance == null)
                instance = CreateInstance(prefab);

            instance.position = position;
            instance.rotation = rotation;
            instance.gameObject.SetActive(autoActivate);

            return instance;
        }

        private Transform FindInstance(Transform prefab)
        {
            if (_instances.ContainsKey(prefab))
                return _instances[prefab].FirstOrDefault(t => !t.gameObject.activeSelf);

            return null;
        }

        private Transform CreateInstance(Transform prefab)
        {
            var t = MonoBehaviour.Instantiate(prefab);
            t.gameObject.SetActive(false);

            if (!_instances.ContainsKey(prefab))
                _instances.Add(prefab, new List<Transform>());

            _instances[prefab].Add(t);
            return t;
        }
    }
}