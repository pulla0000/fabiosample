﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MagicianGames
{
    public interface IScenesLoader
    {
        void LoadScene(string sceneName, bool showLoading = true, bool allowSceneActivation = true);
        void AllowSceneActivation(bool showLoading = true);
    }
}