﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.ResourceManagement.ResourceProviders;

namespace MagicianGames.Core.LoadingScreen
{
    public class ScenesLoaderController : IScenesLoader
    {
        private const string TAG = "#SCENE LOADER# ";
        private SceneInstance? _sceneReq;

        private AssetReference _viewReference;
        private GameObject _view;

        private bool _isLoading;

        public ScenesLoaderController(AssetReference view)
        {
            _viewReference = view;
            SceneManager.sceneLoaded += OnSceneLoaded;
        }

        private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
        {
            _sceneReq = null;
            _isLoading = false;
            UnloadView();
        }

        public void LoadScene(string sceneName, bool showLoading = true, bool allowSceneActivation = true)
        {
            if (_isLoading)
            {
                Debug.LogError(TAG + " Can't load scene " + sceneName + " because ona scene is already beeing loading");
                return;
            }

            if (showLoading)
                LoadView();

            Debug.Log(TAG + "LoadScene " + sceneName);
            _isLoading = true;
            Addressables.LoadSceneAsync(sceneName, activateOnLoad: allowSceneActivation).Completed += OnLoadSceneComplete;
        }

        private void OnLoadSceneComplete(AsyncOperationHandle<SceneInstance> req)
        {
            _sceneReq = req.Result;
        }

        public void AllowSceneActivation(bool showLoading = true)
        {
            if (_sceneReq == null)
                return;

            if (showLoading)
                LoadView();

            Debug.Log(TAG + "AllowSceneActivation()");
            _sceneReq.Value.Activate();
            _sceneReq = null;
        }

        #region View

        private void LoadView()
        {
            if (_view != null)
                return;

            Debug.Log(TAG + "Loading View");
            Addressables.InstantiateAsync(_viewReference, Vector3.zero, Quaternion.identity).
            Completed +=
            (req) =>
            {
                Debug.Log(TAG + "View Loaded");
                _view = req.Result;
            };
        }

        private void UnloadView()
        {
            if (_view != null)
                Addressables.ReleaseInstance(_view);
        }

        #endregion
    }
}