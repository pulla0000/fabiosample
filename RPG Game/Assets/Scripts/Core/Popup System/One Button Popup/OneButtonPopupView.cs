﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using DG.Tweening;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;

namespace MagicianGames.Core.Popup.OneButtonPopup
{
    public class OneButtonPopupView : MonoBehaviour
    {
        [SerializeField]
        private GameObject _window;

        [SerializeField]
        private TextMeshProUGUI _message, _buttonText;
        
        [SerializeField]
        private InputActionReference _buttonActions;
        
        private bool _clicked;
        
        public event System.Action OnButtonClicked;

        private void Awake()
        {
        }

        private void EnableInput()
        {
            _buttonActions.action.performed += OnClick;
            _buttonActions.action.Enable();
            
            EventSystem.current.sendNavigationEvents = false;
        }

        private void DisableInput()
        {
            _buttonActions.action.performed -= OnClick;
            _buttonActions.action.Disable();
            
            EventSystem.current.sendNavigationEvents = true;
        }
        
        public void Show(string message, string buttonText)
        {
            _message.text = message;
            _buttonText.text = buttonText;
            _window.SetActive(true);
            _clicked = false;
            EnableInput();
        }

        private void Close()
        {
            DisableInput();
            _window.SetActive(false);
        }

        public void OnClick(InputAction.CallbackContext ctx)
        {
            if (_clicked)
                return;

            _clicked = true;
            OnButtonClicked?.Invoke();
            Close();
        }
    }
}