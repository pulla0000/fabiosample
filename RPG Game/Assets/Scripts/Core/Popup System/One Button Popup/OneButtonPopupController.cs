﻿using UnityEngine.AddressableAssets;

namespace MagicianGames.Core.Popup.OneButtonPopup
{
    public class OneButtonPopupController : IOneButtonPopup
    {
        private OneButtonPopupView _view;
        private AssetReference _defaultReference, _overrideReference;

        public bool IsOpen => _view != null;

        private AssetReference ViewReference => _overrideReference ?? _defaultReference;

        public OneButtonPopupController(AssetReference viewReference)
        {
            _defaultReference = viewReference;
        }
      
        public void OverrideGraphics(AssetReference viewReference)
        {
            _overrideReference = viewReference;
        }

        public async void Open(string message, string buttonText, System.Action buttonCallback = null)
        {
            if (_view == null)
                _view = await AddressablesHelper.Instantiate<OneButtonPopupView>(ViewReference);

            _view.OnButtonClicked +=
                () =>
                {
                    Close();
                    buttonCallback?.Invoke();
                };
            _view.Show(message, buttonText);
        }

        public void Close()
        {
            UnloadView();
        }

        private void UnloadView()
        {
            if (_view != null)
                Addressables.ReleaseInstance(_view.gameObject);
        }
    }
}