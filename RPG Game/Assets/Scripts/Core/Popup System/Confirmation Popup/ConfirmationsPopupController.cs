﻿using System.Collections;
using Core.RoutineRunner;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.EventSystems;

namespace MagicianGames.Core.Popup.TwoButtonsPopup
{
    public class ConfirmationsPopupController : IConfirmationPopup
    {
        private ConfirmationPopupView _view;
        private AssetReference _defaultReference, _overrideReference;

        public bool IsOpen => _view != null;

        private AssetReference ViewReference => _overrideReference ?? _defaultReference;

        public ConfirmationsPopupController(AssetReference viewReference)
        {
            _defaultReference = viewReference;
        }

        public void OverrideGraphics(AssetReference viewReference)
        {
            _overrideReference = viewReference;
        }

        public async void Open(string message, System.Action<int> buttonCallback = null, params string[] buttonsText)
        {
            if (buttonsText.Length < 1)
            {
                UnityEngine.Debug.LogError("#Confirmation Popup# Can't display popup without button");
            }
            
            if (_view == null)
                _view = await AddressablesHelper.Instantiate<ConfirmationPopupView>(ViewReference);

            _view.OnButtonClicked +=
                (button) =>
                {
                    Close();
                    buttonCallback?.Invoke(button);
                };

            _view.Show(message, buttonsText);
        }

        public void Close()
        {
            UnloadView();
        }

        private void UnloadView()
        {
            if (_view != null)
                Addressables.ReleaseInstance(_view.gameObject);
        }

    }
}