﻿using System;
using UnityEngine;
using TMPro;
using DG.Tweening;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;

namespace MagicianGames.Core.Popup.TwoButtonsPopup
{
    public class ConfirmationPopupView : MonoBehaviour
    {
        [SerializeField]
        private GameObject _window;

        [SerializeField] private TextMeshProUGUI _message;

        [SerializeField]
        private ConfirmationPopupButton[] _buttons;
      
        private bool _clicked;

        [SerializeField]
        private InputActionReference[] _buttonsActions;

        private Action<InputAction.CallbackContext>[] _buttonsCallback;

        public event System.Action<int> OnButtonClicked;
        
        private void Awake()
        {
            //_window.SetActive(false);
            _buttonsCallback = new Action<InputAction.CallbackContext> [_buttons.Length];

            for (int i = 0; i < _buttonsCallback.Length; i++)
            {
                int index = i;
                _buttonsCallback[i] =ctx => OnClick(index); 
            }

            ResetButtons();
        }

        private void EnableInput()
        {
            EventSystem.current.sendNavigationEvents = false;

            for (int i = 0; i < _buttonsActions.Length; i++)
            {
                var action = _buttonsActions[i];
                var callback = _buttonsCallback[i];

                action.action.performed += callback;
                action.action.Enable();
            }
        }

        private void DisableInput()
        {
            for (int i = 0; i < _buttonsActions.Length; i++)
            {
                var action = _buttonsActions[i];
                var callback = _buttonsCallback[i];

                action.action.performed -= callback;
                action.action.Disable();
            }
            
            EventSystem.current.sendNavigationEvents = true;
        }
        
        public void Show(string message, params string[] buttonsText)
        {
            _message.text = message;

            for (int i = 0; i < buttonsText.Length; i++)
            {
                var button = GetButton(i);
                button.Setup(buttonsText[i]);
            }
            
            _window.SetActive(true);
            _clicked = false;
            EnableInput();
        }

        private void Close()
        {
            DisableInput();
            ResetButtons();
            _window.SetActive(false);
        }

        private ConfirmationPopupButton GetButton(int index)
        {
            var button = _buttons[index];
            button.gameObject.SetActive(true);
            return button;
        }

        private void ResetButtons()
        {
            foreach (var button in _buttons)
            {
                button.gameObject.SetActive(false);
            }
        }
        
        public void OnClick(int button)
        {
            if (_clicked)
                return;

            _clicked = true;
            OnButtonClicked?.Invoke(button);
            Close();
        }

    }
}