﻿using TMPro;
using UnityEngine;

namespace MagicianGames.Core.Popup.TwoButtonsPopup
{
    public class ConfirmationPopupButton : MonoBehaviour
    {
        [SerializeField]
        private TextMeshProUGUI _buttonValue, _buttonText;

        public void Setup(/*string buttonValue,*/ string buttonText)
        {
            /*_buttonValue.text = buttonValue;*/
            _buttonText.text = buttonText;
        }
    }
}