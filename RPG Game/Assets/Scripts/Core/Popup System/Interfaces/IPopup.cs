﻿using UnityEngine.AddressableAssets;

namespace MagicianGames
{
    public interface IPopup
    {
        bool IsOpen { get; }

        /// <summary>
        /// Override the default graphics of this popup
        /// </summary>
        /// <param name="viewReference"></param>
        void OverrideGraphics(AssetReference viewReference);
    }
}