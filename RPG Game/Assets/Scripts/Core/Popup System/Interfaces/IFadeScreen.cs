﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MagicianGames.AdventureGame
{
    public interface IFadeScreen: IPopup
    {
        void FadeIn(System.Action onComplete=null);
        void FadeOut(float delay = 0, System.Action onComplete = null);
        void Close();
    }
}