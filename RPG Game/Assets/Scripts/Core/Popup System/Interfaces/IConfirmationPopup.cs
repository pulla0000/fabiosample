﻿namespace MagicianGames
{
    public interface IConfirmationPopup : IPopup
    {
        /// <summary>
        /// 
        /// </summary>       
        /// <param name="buttonCallback">Return 1 if clicked on first button or 2 if clicked on second button</param>
        void Open(string message, System.Action<int> buttonCallback = null, params string[] buttonsText);
        void Close();
    }
}