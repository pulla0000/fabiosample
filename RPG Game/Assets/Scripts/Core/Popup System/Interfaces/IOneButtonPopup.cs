﻿namespace MagicianGames
{
    public interface IOneButtonPopup : IPopup
    {
        void Open(string message, string buttonText, System.Action buttonCallback = null);
        void Close();
    }
}