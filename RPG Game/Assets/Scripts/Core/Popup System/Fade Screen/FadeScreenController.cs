﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.AddressableAssets;

namespace MagicianGames.AdventureGame.Game.UI.FadeScreen
{
    public class FadeScreenController : IFadeScreen
    {
        private FadeScreenView _view;
        private AssetReference _defaultReference, _overrideReference;

        public bool IsOpen => _view != null;
        private AssetReference ViewReference => _overrideReference ?? _defaultReference;

        public FadeScreenController(AssetReference viewReference)
        {
            _defaultReference = viewReference;
        }

        public void OverrideGraphics(AssetReference viewReference)
        {
            _overrideReference = viewReference;
        }

        public async void FadeIn(Action onComplete = null)
        {
            if (_view == null)
                _view = await AddressablesHelper.Instantiate<FadeScreenView>(ViewReference);

            _view.FadeImage(1, 0, onComplete);
        }

        public async void FadeOut(float delay = 0, Action onComplete = null)
        {
            if (_view == null)
                _view = await AddressablesHelper.Instantiate<FadeScreenView>(ViewReference);

            _view.FadeImage(0, delay, onComplete);
        }

        public void Close()
        {
            UnloadView();
        }

        #region Addressables

        private void UnloadView()
        {
            if (_view != null)
                Addressables.ReleaseInstance(_view.gameObject);
        }

        #endregion
    }
}