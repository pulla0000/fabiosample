﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

namespace MagicianGames.AdventureGame.Game.UI.FadeScreen
{
    public class FadeScreenView : MonoBehaviour
    {
        [SerializeField]
        private Image _fadeImage;

        public void FadeImage(float alpha, float delay, System.Action onComplete)
        {
            _fadeImage.DOFade(alpha, .5f).SetDelay(delay).SetEase(Ease.Linear).OnComplete(() => { onComplete?.Invoke(); });
        }
    }
}