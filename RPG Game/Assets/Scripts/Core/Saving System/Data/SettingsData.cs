﻿using UnityEngine;

namespace MagicianGames.Core.SavingSystem
{
    [System.Serializable]
    public class SettingsData
    {
        public int MusicVolume = 0, SfxVolume = 0;
        public int Language;
        public ResolutionData Resolution = 
                new ResolutionData()
                {
                    Width = Screen.currentResolution.width,
                    Height = Screen.currentResolution.height,
                    RefreshRate = Screen.currentResolution.refreshRate
                };
        
        public bool FullScreen = Screen.fullScreen;

        public void SetResolution(Resolution resolution)
        {
            Resolution = 
                new ResolutionData()
                {
                    Width = resolution.width,
                    Height = resolution.height,
                    RefreshRate = Resolution.RefreshRate
                };
        }
        
        public void SetRefreshRate(int refreshRate)
        {
            Resolution = 
                new ResolutionData()
                {
                    Width = Resolution.Width,
                    Height = Resolution.Height,
                    RefreshRate = refreshRate
                };
        }

        [System.Serializable]
        public struct ResolutionData
        {
            public int Width;
            public int Height;
            public int RefreshRate;
        }
    }
}