﻿using MagicianGames.Core.QuestSystem;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace MagicianGames.Core.SavingSystem
{
    [System.Serializable]
    public class QuestData
    {
        public List<QuestSavableData> Quests;

        public QuestData()
        {
            Quests = new List<QuestSavableData>();
        }             

        public QuestSavableData GetData(string id)
        {
            return Quests.FirstOrDefault(q => q.Id == id);
        }
    }
}