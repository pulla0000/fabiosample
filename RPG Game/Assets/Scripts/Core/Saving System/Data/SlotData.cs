﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MagicianGames.Core.SavingSystem
{
    [System.Serializable]
    public class SlotData
    {
        public int Id;
        public float GameTime;
        public GameData GameData;

        public SlotData()
        {
        }
        
        public SlotData(int id)
        {
            Id = id;
            GameTime = 0;
            GameData = new GameData();
        }

        public void Delete()
        {
            GameTime = 0;
            GameData = new GameData();
        }
    }
}