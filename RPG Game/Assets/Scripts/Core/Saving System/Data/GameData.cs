﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MagicianGames.Core.SavingSystem
{
    [System.Serializable]
    public class GameData
    {
        public string CurrentScene;
        public QuestData QuestData;

        public GameData()
        {
            QuestData = new QuestData();
        }
    }
}