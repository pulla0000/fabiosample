﻿using MagicianGames.Core.SavingSystem;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MagicianGames
{
    public interface ISettingsData
    {
        SettingsData Settings { get; }
        void Save();
        void Load();
    }
}