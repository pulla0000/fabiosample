﻿using MagicianGames.Core.SavingSystem;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MagicianGames
{
    public interface IGameData
    {
        GameData GameData { get; }
        void AddGameTime(float time);
        void SetSlot(SlotData slot);
        void Save();
    }
}