﻿using Core.Audio_System;
using Core.Environment;
using UnityEngine;

namespace MagicianGames.Core.SavingSystem
{
    public class SettingsDataManager : ISettingsData
    {
        private const string FILE_NAME = "settings";

        public SettingsData Settings { get; private set; }

        private IMasterAudioSystem _masterAudioSystem;
        
        public SettingsDataManager()
        {
        }

        [Zenject.Inject]
        public void InjectMasterAudio(IMasterAudioSystem masterAudioSystem)
        {
            _masterAudioSystem = masterAudioSystem;
            if (!Application.isPlaying)
            {
                return;
            }
            
            Load();
        }
        
        public async void Load()
        {
            if (FileHelper.HasFile(FILE_NAME))
                Settings = await FileHelper.LoadFromJsonFileAsync<SettingsData>(FILE_NAME, EnvironmentVariables.EncryptData());
            else
                Settings = new SettingsData();
            
            ApplySettings();
        }

        private void ApplySettings()
        {
            Screen.SetResolution(Settings.Resolution.Width, Settings.Resolution.Height, Settings.FullScreen);
            
            _masterAudioSystem.TryChangeParameter(AudioConstants.MUSIC_VOLUME_KEY, Settings.MusicVolume);
            _masterAudioSystem.TryChangeParameter(AudioConstants.SFX_VOLUME_KEY, Settings.SfxVolume);
        }
        
        public void Save()
        {
            FileHelper.SaveToJsonFile(Settings, FILE_NAME,EnvironmentVariables.EncryptData());
        }
    }
}