﻿using System.Threading.Tasks;
using Core.Environment;

namespace MagicianGames.Core.SavingSystem
{
    public static class SlotLoader
    {
        private const string FILE_NAME = "S_";

        public static async Task<SlotData> LoadSlotAsync(int id)
        { 
            var slot = await FileHelper.LoadFromJsonFileAsync<SlotData>(FILE_NAME + id, EnvironmentVariables.EncryptData());

            if (slot == null)
            {
                slot = new SlotData(id);
            }

            return slot;
        }

        public static SlotData LoadSlot(int id)
        {
            var slot = FileHelper.LoadFromJsonFile<SlotData>(FILE_NAME + id, EnvironmentVariables.EncryptData());

            if (slot == null)
            {
                slot = new SlotData(id);
            }

            return slot;
        }
        
        public static void SaveSlot(SlotData slot)
        {
            FileHelper.SaveToJsonFile(slot, FILE_NAME + slot.Id, EnvironmentVariables.EncryptData());
        }
    }
}