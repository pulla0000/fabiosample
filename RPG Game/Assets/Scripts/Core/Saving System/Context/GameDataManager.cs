﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading.Tasks;

namespace MagicianGames.Core.SavingSystem
{
    public class GameDataManager : IGameData
    {
        private SlotData _slot;

        public GameData GameData
        {
            get
            {
                if (_slot == null)
                {
                    _slot = SlotLoader.LoadSlot(-1);
                }

                return _slot.GameData;
            }
        }

        public void AddGameTime(float time)
        {
            if (_slot == null)
                _slot = new SlotData(-1);

            _slot.GameTime += time;
        }

        public void SetSlot(SlotData slot)
        {
            _slot = slot;
        }

        public void Save()
        {
            SlotLoader.SaveSlot(_slot);
        }
    }
}