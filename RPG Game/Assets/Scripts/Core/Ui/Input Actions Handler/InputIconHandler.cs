﻿using System;
using System.Collections.Generic;
using Core.Input_System.Data;
using Core.InputSystem;
using MagicianGames;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;

namespace Core.Ui
{
    public class InputIconHandler : MonoBehaviour
    {
        [SerializeField]
        private InputActionReference _inputAction;

        [SerializeField]
        private Image _buttonImage = default;
        
        [Zenject.Inject]
        private IInputSystem _inputSystem;

        [Zenject.Inject]
        private InputSpriteData _spriteData;

        public void SetInputAction(InputActionReference inputAction)
        {
            _inputAction = inputAction;
            ChangeSchemeTo(_inputSystem.GetCurrentControllerType(),_inputSystem.GetPairedDevices());
        }
        
        private void OnEnable()
        {
            NotificationsManager.AddListener<ControllerInputChangedNotification>(OnInputDeviceChange);
            ChangeSchemeTo(_inputSystem.GetCurrentControllerType(),_inputSystem.GetPairedDevices());
        }

        private void OnDisable()
        {
            NotificationsManager.RemoveListener<ControllerInputChangedNotification>(OnInputDeviceChange);
        }

        private void OnInputDeviceChange(ControllerInputChangedNotification notification)
        {
            ChangeSchemeTo(notification.NewController, notification.PairedDevices);
        }

        private void ChangeSchemeTo(string scheme, IReadOnlyList<string> devices)
        {
            if (_inputAction == null || string.IsNullOrEmpty(scheme))
            {
                return;
            }

            InputBinding? schemeBinding = null;

            foreach (var binding in _inputAction.action.bindings)
            {
                if (binding.groups.Contains(scheme))
                {
                    schemeBinding = binding;
                    break;
                }
            }

            if (!schemeBinding.HasValue)
            {
                Debug.LogErrorFormat("No binding found in the action {0} for scheme {1}", _inputAction.action.name, scheme);
                return;
            }

            var deviceName = devices[0];
            
            if(_spriteData.TryGetButtonSprite(deviceName,schemeBinding.Value.effectivePath, out Sprite sprite))
            {
                _buttonImage.sprite = sprite;
            }
        }
    }
}