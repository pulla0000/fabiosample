﻿namespace Core.Ui.Interfaces
{
    public interface IInputActionHandler
    {
        void EnableInput();
        void DisableInput();
    }
}