﻿using System;
using Core.Ui.Interfaces;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;

namespace Core.Ui
{
    public class InputActionHandler : MonoBehaviour, IInputActionHandler
    {
        [SerializeField]
        private InputActionData[] _inputActions;

        private void Awake()
        {
            foreach (var action in _inputActions)
            {
                if (action.InputIconHandler == null)
                {
                    continue;
                }
                
                action.InputIconHandler.SetInputAction(action.InputAction);
            }
        }

        public void EnableInput()
        {
            foreach (var action in _inputActions)
            {
                action.InputAction.action.performed += action.InputActionEventCallback.Invoke;
                action.InputAction.action.Enable();
            }
        }

        public void DisableInput()
        {
            foreach (var action in _inputActions)
            {
                action.InputAction.action.performed -= action.InputActionEventCallback.Invoke;
                action.InputAction.action.Disable();
            }
        }
        
        [System.Serializable]
        private struct InputActionData
        {
            public string ActionName;
            public InputIconHandler InputIconHandler;
            public InputActionReference InputAction;
            public InputActionEvent InputActionEventCallback;

            [System.Serializable]
            public class InputActionEvent: UnityEvent<InputAction.CallbackContext> { }
        }
    }
}