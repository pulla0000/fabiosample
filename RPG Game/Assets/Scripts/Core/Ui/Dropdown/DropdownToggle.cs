﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Core.Ui
{
    public class DropdownToggle : Toggle
    {
        private DropdownAutoScroll _dropdown;

        private void Initialize()
        {
            if (_dropdown != null)
            {
                return;
            }

            _dropdown = GetComponentInParent<DropdownAutoScroll>();
        }

        public override void OnSelect(BaseEventData eventData)
        {
            Initialize();
            base.OnSelect(eventData);
            _dropdown.OnUpdateSelected(eventData.selectedObject);
        }
    }
}