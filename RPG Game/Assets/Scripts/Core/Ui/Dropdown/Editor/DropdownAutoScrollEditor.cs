﻿using UnityEditor;
using TMPro.EditorUtilities;

namespace Core.Ui.Editor
{
    [CustomEditor(typeof(DropdownAutoScroll))]
    public class DropdownAutoScrollEditor : TMPro.EditorUtilities.DropdownEditor
    {
        private SerializedProperty _scrollMargin;

        protected override void OnEnable()
        {
            _scrollMargin = serializedObject.FindProperty("_scrollMargin");
            base.OnEnable();
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            EditorGUILayout.PropertyField(_scrollMargin);
        }
    }
}