﻿using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Core.Ui
{
    public class DropdownAutoScroll : TMP_Dropdown
    {
        [SerializeField]
        private float _scrollMargin = 0.5f; // how much to "overshoot" when scrolling, relative to the selected item's height
        
        private ScrollRect _scrollRect;
        private bool _forceUpdate;
        private Coroutine _updateCoroutine;
        
        protected override GameObject CreateDropdownList(GameObject template)
        {
            var go = base.CreateDropdownList(template);
            _scrollRect = go.GetComponent<ScrollRect>();
            _forceUpdate = true;
            return go;
        }

        public override void Select()
        {
            if (!IsExpanded)
            {
                return;
            }
            
            base.Select();
        }

        public void OnUpdateSelected(GameObject selectedObject)
        {
            if (_updateCoroutine != null)
            {
                StopCoroutine(_updateCoroutine);
            }

            _updateCoroutine = StartCoroutine(OnUpdateSelectedCoroutine(selectedObject));
        }

        public IEnumerator OnUpdateSelectedCoroutine(GameObject selectedObject)
        {
            if (_scrollRect == null)
            {
                yield break;
            }

            if (_forceUpdate)
            {
                yield return 0;
            }

            // helper vars
            float contentHeight = _scrollRect.content.sizeDelta.y;
            float viewportHeight = _scrollRect.viewport.rect.height;
            
            // what bounds must be visible?
            float centerLine = selectedObject.transform.localPosition.y; // selected item's center
            float upperBound = centerLine + (selectedObject.GetComponent<RectTransform>().rect.height / 2f); // selected item's upper bound
            float lowerBound = centerLine - (selectedObject.GetComponent<RectTransform>().rect.height / 2f); // selected item's lower bound
            
            // what are the bounds of the currently visible area?
            float lowerVisible = (contentHeight - viewportHeight) * _scrollRect.normalizedPosition.y - contentHeight;
            float upperVisible = lowerVisible + viewportHeight;
            
            float desiredLowerBound;
            if (upperBound > upperVisible) {
                // need to scroll up to upperBound
                desiredLowerBound = upperBound - viewportHeight + selectedObject.GetComponent<RectTransform>().rect.height * _scrollMargin;
            } else if (lowerBound < lowerVisible) {
                // need to scroll down to lowerBound
                desiredLowerBound = lowerBound - selectedObject.GetComponent<RectTransform>().rect.height * _scrollMargin;
            } else {
                // item already visible - all good
                yield break;
            }
            
            // normalize and set the desired viewport
            float normalizedDesired = (desiredLowerBound + contentHeight) / (contentHeight - viewportHeight);
            _scrollRect.normalizedPosition = new Vector2(0f, Mathf.Clamp01(normalizedDesired));

            _updateCoroutine = null;
        }
    }
}