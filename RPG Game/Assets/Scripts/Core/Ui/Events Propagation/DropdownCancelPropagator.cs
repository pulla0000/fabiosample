﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Core.Ui
{
    public class DropdownCancelPropagator: UiCancelPropagator
    {
        private TMP_Dropdown _dropdown;
        private void Awake()
        {
            _dropdown = GetComponent<TMP_Dropdown>();
        }

        public override void OnCancel(BaseEventData eventData)
        {
            if (_dropdown.IsExpanded)
            {
                return;
            }
            
            base.OnCancel(eventData);
        }
    }
}