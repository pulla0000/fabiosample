﻿using System;
using UnityEngine.EventSystems;

namespace Core.Ui
{
    public class UiCancelPropagator : EventTrigger
    {
        private void Start()
        {
            Entry cancelEntry = new Entry(){ eventID = EventTriggerType.Cancel};
            cancelEntry.callback.AddListener(OnCancel);
            triggers.Add(cancelEntry);
        }

        public override void OnCancel(BaseEventData eventData)
        {
            /*base.OnCancel(eventData);*/
            ExecuteEvents.ExecuteHierarchy(transform.parent.gameObject, eventData, ExecuteEvents.cancelHandler);
        }
    }
}