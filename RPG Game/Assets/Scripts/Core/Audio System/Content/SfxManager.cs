﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;
using System.Linq;

namespace MagicianGames.Core.AudioSystem
{
    public class SfxManager : MonoBehaviour, ISfxManager
    {
        [System.Serializable]
        public struct AudioData
        {
            public string Key;
            public AssetReference Audio;
            public bool PreLoad;
            public bool AutoUnload;
        }

        [SerializeField]
        private int _audioSourcesToPool = 5;

        [SerializeField]
        private AudioData[] _audiosData;

        [SerializeField]
        private AudioSource _audioSourcePrefab;

        [SerializeField]
        private Transform _audioSourcesParent;

        [Zenject.Inject]
        private IMasterAudioSystem _masterAudioSystem;
        
        private List<AudioSource> _sfxSources;
        private Dictionary<string, AudioData> _audios;
        private Dictionary<string, AudioClip> _preLoadedAudioClips;

        private void Awake()
        {
            _audios = new Dictionary<string, AudioData>();
            _preLoadedAudioClips = new Dictionary<string, AudioClip>();
            _sfxSources = new List<AudioSource>();

            _audioSourcePrefab.gameObject.SetActive(false);
            _audioSourcePrefab.playOnAwake = false;
            PreLoadAudios();

            foreach (var a in _audiosData)
            {
                if (_audios.ContainsKey(a.Key))
                    continue;

                _audios.Add(a.Key, a);
            }

            for (int i = 0; i < _audioSourcesToPool; i++)
            {
                CreateAudioSource();
            }
        }

        private void PreLoadAudios()
        {
            foreach (var a in _audiosData)
            {
                if (_preLoadedAudioClips.ContainsKey(a.Key) || a.Audio == null)
                    continue;

                if (!a.PreLoad || a.AutoUnload)
                    continue;

                var clip = AddressablesHelper.LoadAsset<AudioClip>(a.Audio).Result;
                _preLoadedAudioClips.Add(a.Key, clip);
            }
        }

        #region AudioSource

        /// <summary>
        /// Get an Audio Source
        /// </summary>
        /// <returns>The first available source</returns>
        private AudioSource GetAudioSource()
        {
            AudioSource source = _sfxSources.FirstOrDefault(s => !s.gameObject.activeSelf);

            if (source == null)
                source = CreateAudioSource();

            source.gameObject.SetActive(true);
            return source;
        }

        /// <summary>
        /// Create a new Audio Source
        /// </summary>
        /// <returns>The new Audio Source</returns>
        private AudioSource CreateAudioSource()
        {
            AudioSource source = Instantiate(_audioSourcePrefab);
            source.transform.SetParent(_audioSourcesParent);
            source.transform.localScale = Vector3.one;
            source.gameObject.SetActive(false);
            _sfxSources.Add(source);

            return source;
        }

        #endregion

        #region Player

        public void PlaySfx(string key, string mixerName, bool isLoop = false, System.Action<AudioSource> result = null)
        {
            PlaySfx(key, mixerName, Vector3.zero, isLoop, result);
        }

        public void PlaySfx(string key, string mixerName, Vector3 pos, bool isLoop = false,
            System.Action<AudioSource> result = null)
        {
            if (!_audios.ContainsKey(key))
            {
                Debug.LogError("#AUDIO MANAGER# Audio " + key + " Not Found");
                result?.Invoke(null);
                return;
            }

            if (_preLoadedAudioClips.ContainsKey(key))
            {
                AudioClip clip = _preLoadedAudioClips[key];
                var audioSource = PlaySfx(clip, mixerName, pos, isLoop);
                result?.Invoke(audioSource);
            }
            else
            {
                AssetReference asset = null;
                AudioData data = _audios[key];
                AddressablesHelper.LoadAsset<AudioClip>(data.Audio, audioClip =>
                {
                    if (data.AutoUnload)
                    {
                        asset = data.Audio;
                    }
                    else
                    {
                        _preLoadedAudioClips.Add(data.Key, audioClip);
                    }

                    var audioSource = PlaySfx(audioClip, mixerName, pos, isLoop, asset);
                    result?.Invoke(audioSource);
                });
            }
        }

        public AudioSource PlaySfx(AudioClip clip, string mixerName, bool isLoop = false)
        {
            return PlaySfx(clip, mixerName, Vector3.zero, isLoop);
        }

        public AudioSource PlaySfx(AudioClip clip, string mixerName, Vector3 pos, bool isLoop = false)
        {
            return PlaySfx(clip, mixerName, pos, isLoop, null);
        }

        private AudioSource PlaySfx(AudioClip clip, string mixerName, Vector3 pos, bool isLoop = false, AssetReference assetReference = null)
        {
            var mixerGroup = _masterAudioSystem.GetAudioMixer(mixerName);
            var audioSource = GetAudioSource();
            audioSource.outputAudioMixerGroup = mixerGroup;
            audioSource.clip = clip;
            audioSource.loop = isLoop;
            audioSource.Play();

            StartCoroutine(DisableSource(audioSource,assetReference));
            return audioSource;
        }
        
        public void StopSfx(AudioSource source)
        {
            source.Stop();
            source.gameObject.SetActive(false);
        }

        #endregion

        /// <summary>
        /// Disable the Audio Source after it plays the sound
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        private IEnumerator DisableSource(AudioSource source, AssetReference assetReference = null)
        {
            while (source.isPlaying)
                yield return 0;

            assetReference?.ReleaseAsset();
            source.gameObject.SetActive(false);
        }

    }
}