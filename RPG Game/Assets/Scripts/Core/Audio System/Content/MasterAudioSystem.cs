﻿using System;
using UnityEngine;
using UnityEngine.Audio;

namespace MagicianGames.Core.AudioSystem
{
    public class MasterAudioSystem : IMasterAudioSystem
    {
        private readonly AudioMixer _masterMixer;
        
        public MasterAudioSystem(AudioMixer masterMixer)
        {
            _masterMixer = masterMixer;
        }

        /// <summary>
        /// Returns the first audio group from Master Mixer with the given name
        /// </summary>
        /// <param name="groupName"></param>
        /// <returns></returns>
        public AudioMixerGroup GetAudioMixer(string groupName)
        {
            if (string.IsNullOrEmpty(groupName))
            {
                Debug.LogErrorFormat("#Master Audio System# can't look for an empty group name");
                return null;
            }

            var groups =  _masterMixer.FindMatchingGroups(groupName);

            if (groups.Length < 1)
            {
                Debug.LogErrorFormat("#Master Audio System# No group found with name {0}", groupName);
                return null;
            }
            
            if (groups.Length > 1)
            {
                Debug.LogErrorFormat("#Master Audio System# Found more than one group with name {0}", groupName);
            }

            return groups[0];
        }

        public bool TryChangeParameter(string parameter, float value)
        {
            try
            {
                _masterMixer.SetFloat(parameter, value);
                return true;
            }
            catch(Exception e)
            {
                Debug.LogErrorFormat("#Master Audio System# Error on change parameter {0}, Error: {1}",parameter,e.Message);
                return false;
            }
        }
    }
}