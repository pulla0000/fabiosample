﻿namespace Core.Audio_System
{
    public class AudioConstants
    {
        public const string SFX_VOLUME_KEY = "Volume_Sfx";
        public const string MUSIC_VOLUME_KEY = "Volume_Music";
    }
}