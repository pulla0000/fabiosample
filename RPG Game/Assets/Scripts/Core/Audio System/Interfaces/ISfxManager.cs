﻿using UnityEngine;

namespace MagicianGames
{
    public interface ISfxManager
    {
        void PlaySfx(string key, string mixerName, bool isLoop = false, System.Action<AudioSource> result = null);
        void PlaySfx(string key, string mixerName, Vector3 pos, bool isLoop = false, System.Action<AudioSource> result = null);
        AudioSource PlaySfx(AudioClip clip, string mixerName, bool isLoop = false);
        AudioSource PlaySfx(AudioClip clip, string mixerName, Vector3 pos, bool isLoop = false);
        void StopSfx(AudioSource source);
    }
}