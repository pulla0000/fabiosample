﻿using UnityEngine.Audio;

namespace MagicianGames
{
    public interface IMasterAudioSystem
    {
        AudioMixerGroup GetAudioMixer(string groupName);
        bool TryChangeParameter(string parameter, float value);
    }
}