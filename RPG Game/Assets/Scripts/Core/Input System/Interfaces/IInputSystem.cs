﻿using System;
using System.Collections.Generic;
using Core.InputSystem.Gamepads;
using UnityEngine.InputSystem;

namespace Core.InputSystem
{
    public interface IInputSystem: IDisposable
    {
        string GetCurrentControllerType();
        IReadOnlyList<string> GetPairedDevices();
        AGamepad GetGamepad();
    }
}