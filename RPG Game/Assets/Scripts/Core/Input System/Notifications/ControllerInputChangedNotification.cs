﻿using System.Collections.Generic;
using MagicianGames;

namespace Core.InputSystem
{
    public readonly struct ControllerInputChangedNotification : INotification
    {
        public string NewController { get; }
        public IReadOnlyList<string> PairedDevices { get; }
        
        public ControllerInputChangedNotification(string newScheme, List<string> pairedDevices)
        {
            NewController = newScheme;
            PairedDevices = pairedDevices;
        }
    }
}