﻿using System;
using Core.InputSystem.LightPatterns;
using Core.InputSystem.RumblePatterns;
using UnityEngine.InputSystem;

namespace Core.InputSystem.Gamepads
{
    public abstract class AGamepad
    {
        protected Gamepad _gamepad;
        protected IRumblePattern _currentRumble;
        protected ILightPattern _currentLight;

        public virtual void StartRumble(IRumblePattern pattern)
        {
            if (_gamepad == null || pattern == null)
            {
                return;
            }

            if (_currentRumble != null)
            {
                StopRumble();
            }
            
            _currentRumble = pattern;
            _currentRumble.Rumble(_gamepad, OnRumbleCompleted);
        }

        public virtual void StopRumble()
        {
            if (_gamepad == null || _currentRumble == null)
            {
                return;
            }

            _currentRumble.Stop(_gamepad);
            _currentRumble = null;
        }

        protected virtual void OnRumbleCompleted()
        {
            _currentRumble = null;
        }

        public virtual void ChangeLight(ILightPattern lightPattern, Action onComplete = null)
        {
        }
    }
}