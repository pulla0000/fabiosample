﻿using System;
using Core.InputSystem.LightPatterns;
using Core.InputSystem.RumblePatterns;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.DualShock;

namespace Core.InputSystem.Gamepads
{
    public class DualShockController: AGamepad
    {
        private readonly DualShockGamepad _dualShockController;
        private Action _onComplete;
        public DualShockController(DualShockGamepad gamepad)
        {
            _dualShockController = gamepad;
        }

        public override void ChangeLight(ILightPattern lightPattern, Action onComplete = null)
        {
            if (_dualShockController == null || lightPattern == null)
            {
                return;
            }
            
            _currentLight = lightPattern;
            _onComplete = onComplete;
            _currentLight.SetLight(_dualShockController, OnLightCompleted);
        }

        private void OnLightCompleted()
        {
            _currentLight = null;
            _onComplete?.Invoke();
            _onComplete = null;
        }
    }
}