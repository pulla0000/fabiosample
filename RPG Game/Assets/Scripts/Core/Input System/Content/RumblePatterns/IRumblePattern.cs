﻿using System;
using UnityEngine.InputSystem;

namespace Core.InputSystem.RumblePatterns
{
    public interface IRumblePattern
    {
        void Rumble(Gamepad gamepad, Action onComplete);
        void Stop(Gamepad gamepad);
    }
}