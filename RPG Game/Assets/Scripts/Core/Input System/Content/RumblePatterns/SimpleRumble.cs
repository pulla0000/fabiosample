﻿using System;
using UnityEngine.InputSystem;
using DG.Tweening;

namespace Core.InputSystem.RumblePatterns
{
    public class SimpleRumble:IRumblePattern
    {
        private readonly float _force;
        private readonly float _duration;
        
        public SimpleRumble(float force, float duration)
        {
            _force = force;
            _duration = duration;
        }

        public void Rumble(Gamepad gamepad, Action onComplete)
        {
            float time = _duration;

            DOTween.To(() => time, t => time = t, 0, _duration).
                OnStart(() =>
                {
                    gamepad.SetMotorSpeeds(_force, _force);
                }).
                OnComplete(() =>
                {
                    gamepad.SetMotorSpeeds(0, 0);
                    onComplete?.Invoke();
                });
        }

        public void Stop(Gamepad gamepad)
        {
            gamepad.SetMotorSpeeds(0, 0);
        }
    }
}