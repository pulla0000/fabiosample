﻿using System.Collections.Generic;
using System.Linq;
using Core.InputSystem.Gamepads;
using MagicianGames;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.DualShock;
using UnityEngine.InputSystem.LowLevel;
using UnityEngine.InputSystem.Users;

namespace Core.InputSystem
{
    public class InputSystem: IInputSystem
    {
        private readonly InputActionAsset _actionMap;
        private InputUser _inputUser;

        private string _currentScheme;
        private List<string> _pairedDevices;
        private AGamepad _gamepad;
        
        public string GetCurrentControllerType() => _currentScheme;
        public IReadOnlyList<string> GetPairedDevices() => _pairedDevices;

        public AGamepad GetGamepad() => _gamepad;
        
        public InputSystem(InputActionAsset actionMap)
        {
#if UNITY_EDITOR
            if (!Application.isPlaying)
            {
                return;
            }
#endif
            _actionMap = actionMap;
            _inputUser =  InputUser.CreateUserWithoutPairedDevices();

            InputUser.onChange += OnUserChange;
            InputUser.onUnpairedDeviceUsed += OnUnpairedDeviceUsed;
            ++InputUser.listenForUnpairedDeviceActivity;
        }

        private void OnUserChange(InputUser user, InputUserChange change, InputDevice device)
        {
            if(change == InputUserChange.DeviceLost)
            {
                _inputUser =  InputUser.CreateUserWithoutPairedDevices();
            }
        }

        private void OnUnpairedDeviceUsed(InputControl control, InputEventPtr e)
        {   
            if (InputControlScheme.FindControlSchemeForDevices(InputUser.GetUnpairedInputDevices(), _actionMap.controlSchemes,
                out var controlScheme, out var matchResult, mustIncludeDevice: control.device))
            {
                try
                {
                    // First remove the currently paired devices.
                    _inputUser.UnpairDevices();
                    
                    // Then pair devices that we've picked according to the control scheme.
                    var newDevices = matchResult.devices;
                    Debug.Assert(newDevices.Count > 0, "Expecting to see at least one device here");
                    for (var i = 0; i < newDevices.Count; ++i)
                    {
                        InputUser.PerformPairingWithDevice(newDevices[i], _inputUser);
                    }

                    _pairedDevices = newDevices.Select(d => d.displayName).ToList();
                    
                    // And finally switch to the new control scheme.
                    _inputUser.ActivateControlScheme(controlScheme);
                    _currentScheme = controlScheme.name;

                    if(Gamepad.all.Count > 0)
                    {
                        var gamepad = Gamepad.all[0];
                        var device = newDevices.FirstOrDefault(d => d == gamepad.device);

                        if (device != null)
                        {
                            PairGamepad(gamepad);
                        }
                    }
                    else
                    {
                        _gamepad?.StopRumble();
                        _gamepad = null;
                    }
                    
                    NotificationsManager.Raise(new ControllerInputChangedNotification(_currentScheme,_pairedDevices));
                }
                finally
                {
                    matchResult.Dispose();
                }
            }
        }

        private void PairGamepad(Gamepad gamepad)
        {
            _gamepad?.StopRumble();
            
            if (gamepad is DualShockGamepad dualShock)
            {
                _gamepad = new DualShockController(dualShock);
            }
            else
            {
                _gamepad = new XboxController(gamepad);
            }
        }

        public void Dispose()
        {
            InputUser.onUnpairedDeviceUsed -= OnUnpairedDeviceUsed;
            --InputUser.listenForUnpairedDeviceActivity;
            _gamepad?.StopRumble();
        }
    }
}