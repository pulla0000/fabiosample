﻿using System;
using UnityEngine;
using DG.Tweening;
using UnityEngine.InputSystem.DualShock;

namespace Core.InputSystem.LightPatterns
{
    public class GamepadBlinkLightPattern: ILightPattern
    {
        private readonly float _blinkSpeed;
        private readonly int _repeatTime;
        private readonly Color _fromColor, _toColor;
        
        public GamepadBlinkLightPattern(Color fromColor, Color toColor, float blinkSpeed = .2f, int repeatTime = 1)
        {
            if (repeatTime < 1)
            {
                repeatTime = 1;
            }
            
            _fromColor = fromColor;
            _toColor = toColor;
            _blinkSpeed = blinkSpeed;
            _repeatTime = repeatTime;
        }

        public void SetLight(DualShockGamepad gamepad, Action onComplete)
        {
            var color = _fromColor;
            
           var tween =  DOTween.To(() => color, c => color = c,_toColor, _blinkSpeed).
                OnStart(() =>
                {
                    gamepad.SetLightBarColor(_fromColor);
                }).
                OnUpdate(() =>
                {
                    gamepad.SetLightBarColor(color);
                }).
                OnComplete(() =>
                {
                    onComplete?.Invoke();
                }).SetLoops(_repeatTime*2, LoopType.Yoyo);
        }
    }
}