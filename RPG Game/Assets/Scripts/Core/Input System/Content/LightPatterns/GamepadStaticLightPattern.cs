﻿using System;
using UnityEngine;
using UnityEngine.InputSystem.DualShock;

namespace Core.InputSystem.LightPatterns
{
    public class GamepadStaticLightPattern:ILightPattern
    {
        private readonly Color _color;
        
        public GamepadStaticLightPattern(Color color)
        {
            _color = color;
        }

        public void SetLight(DualShockGamepad gamepad, Action onComplete)
        {
            gamepad.SetLightBarColor(_color);
        }
    }
}