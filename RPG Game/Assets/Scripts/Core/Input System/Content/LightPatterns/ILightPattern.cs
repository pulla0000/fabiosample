﻿using System;
using UnityEngine.InputSystem.DualShock;

namespace Core.InputSystem.LightPatterns
{
    public interface ILightPattern
    {
        void SetLight(DualShockGamepad gamepad, Action onComplete);
    }
}