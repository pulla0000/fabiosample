﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Core.Input_System.Data
{
    [CreateAssetMenu(fileName = "New Sprite Data", menuName = "Magician Games/Input/Input Sprite Data", order = 0)]
    public class InputSpriteData : ScriptableObject, IDisposable
    {
        [SerializeField]
        private DeviceSpriteData[] _supportedDevices;

        private Dictionary<string, Dictionary<string, Sprite>> _devicesMap;
        
        private void Initialize()
        {
            if (_devicesMap != null)
            {
                return;
            }

            _devicesMap = new Dictionary<string, Dictionary<string, Sprite>>();
            
            foreach (var device in _supportedDevices)
            {
                if (!_devicesMap.ContainsKey(device.DeviceName))
                {
                    _devicesMap.Add(device.DeviceName, new Dictionary<string, Sprite>());
                }

                foreach (var button in device.ButtonSprites)
                {
                    if (!_devicesMap[device.DeviceName].ContainsKey(button.ButtonName))
                    {
                        _devicesMap[device.DeviceName].Add($"<{device.DeviceType}>/{button.ButtonName}", button.ButtonSprite);
                    }
                }
            }
        }
        
        public bool TryGetButtonSprite(string deviceName, string buttonName, out Sprite sprite)
        {
            Initialize();
            return TryGetButtonFromDevice(deviceName, buttonName, out sprite);
        }

        private bool TryGetButtonFromDevice(string deviceName, string buttonName, out Sprite buttonSprite)
        {
            buttonSprite = null;

            if(_devicesMap.TryGetValue(deviceName, out  Dictionary<string, Sprite> deviceData))
            {
                return deviceData.TryGetValue(buttonName, out buttonSprite);
            }
            
            return false;
        }
        
        [System.Serializable]
        private struct DeviceSpriteData
        {
            public string DeviceName;
            public string DeviceType;
            public ButtonSpriteData[] ButtonSprites;
        }
        [System.Serializable]
        private struct ButtonSpriteData
        {
            public string ButtonName;
            public Sprite ButtonSprite;
        }

        public void Dispose()
        {
            _devicesMap?.Clear();
            _devicesMap = null;
        }
    }
}