﻿using MagicianGames.Core.DialogSystem;
using UnityEngine.AddressableAssets;

namespace MagicianGames
{
    public interface IDialogScreen
    {
        bool IsOpen { get; }
        void OverrideGraphics(AssetReference viewReference);
        void Show(DialogData dialog, System.Action onClose);
    }
}