﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MagicianGames.Core.DialogSystem
{
    [CreateAssetMenu(fileName = "New Dialog", menuName = "Magician Games/Dialog/Create New")]
    [System.Serializable]
    public class DialogData : ScriptableObject
    {
        public List<SpeachData> Dialog;

        public DialogData()
        {
            Dialog = new List<SpeachData>();
        }

        [System.Serializable]
        public struct SpeachData
        {
            public string CharacterId;
            public string Text;
        }
    }
}