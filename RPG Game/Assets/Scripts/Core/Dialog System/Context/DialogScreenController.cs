﻿using MagicianGames.AdventureGame;
using MagicianGames.AdventureGame.Game;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.InputSystem;

namespace MagicianGames.Core.DialogSystem
{
    public class DialogScreenController : IDialogScreen
    {
        private DialogScreenView _view;
        private AssetReference _defaultReference, _overrideReference;

        private DialogData _currentDialog;
        private int _currentDialogIndex;

        private UIInput _uiInput;

        [Zenject.Inject]
        private SceneCharactersData _sceneData;

        private System.Action _onClose;

        public bool IsOpen => _view == null ? false : _view.IsOpen;

        private AssetReference ViewReference => _overrideReference ?? _defaultReference;


        public DialogScreenController(AssetReference defaultView)
        {
            _defaultReference = defaultView;
            _uiInput = new UIInput();
        }

        public void OverrideGraphics(AssetReference viewReference)
        {
            _overrideReference = viewReference;
        }

        public async void Show(DialogData dialog, System.Action onClose)
        {
            if (IsOpen)
                return;

            if (_view == null)
                _view = await AddressablesHelper.Instantiate<DialogScreenView>(ViewReference);

            SubscribeListener();
            _onClose = onClose;
            _currentDialog = dialog;
            _currentDialogIndex = 0;
            DisplayDialogText();
        }

        private void SubscribeListener()
        {
            _uiInput.UI.Submit.performed += OnSubmitPressed;
            _uiInput.UI.Submit.Enable();
        }

        private void UnsubscribeListener()
        {
            _uiInput.UI.Submit.performed -= OnSubmitPressed;
            _uiInput.UI.Submit.Disable();
        }

        private void OnSubmitPressed(InputAction.CallbackContext ctx)
        {
            if (_view.IsPlayingAnimation())
                _view.CompleteText();
            else
            {
                var hasNext = IncreaseDialogIndex();

                if (hasNext)
                    DisplayDialogText();
                else
                    Close();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>If has next text</returns>
        private bool IncreaseDialogIndex()
        {
            _currentDialogIndex++;
            return _currentDialogIndex < _currentDialog.Dialog.Count;
        }

        private void DisplayDialogText()
        {
            var image = _sceneData.GetCharacterImage(_currentDialog.Dialog[_currentDialogIndex].CharacterId);
            _view?.Show(_currentDialog.Dialog[_currentDialogIndex].Text, image);
        }

        public void Close()
        {
            UnsubscribeListener();
            _view?.Close();
            _onClose?.Invoke();
            UnloadView();
        }

        private void UnloadView()
        {
            if (_view != null)
                Addressables.ReleaseInstance(_view.gameObject);
        }
    }
}