﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;

namespace MagicianGames.Core.DialogSystem
{
    public class DialogScreenView : MonoBehaviour
    {
        [SerializeField]
        private GameObject _window;

        [SerializeField]
        private TextMeshProUGUI _dialogText;

        [SerializeField]
        private Image _characterImage;

        [SerializeField]
        private float _textAnimationSpeed = 5;

        [Zenject.Inject(Id = "UI Sfx")]
        private ISfxManager _uiSfxManager;
        
        private Tween _textTween;

        public bool IsOpen => _window.activeSelf;

        public bool IsPlayingAnimation()
        {
            if (_textTween != null)
            {
                return /*_textTween.IsComplete() &&*/ _textTween.IsPlaying();
            }

            return false;
        }

        public void CompleteText()
        {
            _textTween?.Kill(true);
            _textTween = null;
        }

        public void Show(string dialogText, Sprite characterSprite = null)
        {
            _dialogText.text = string.Empty;

            _textTween = _dialogText.DOText(dialogText, _textAnimationSpeed).SetSpeedBased(true);
            _characterImage.sprite = characterSprite;
            _window.SetActive(true);
            
            _uiSfxManager.PlaySfx("DialogOpen", "Sfx/UiSfx");
        }

        public void Close()
        {
            _window.SetActive(false);
        }
    }
}