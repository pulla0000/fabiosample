﻿namespace MagicianGames
{
    public interface IScreen
    {
        bool IsOpen { get; }
        void Open();
        void Close();
    }
}