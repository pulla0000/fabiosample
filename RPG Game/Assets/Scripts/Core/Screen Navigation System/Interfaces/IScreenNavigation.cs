﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MagicianGames
{
    public interface IScreenNavigation
    {
        /// <summary>
        /// Register a screen
        /// </summary>
        /// <param name="name">Screen Name</param>
        /// <param name="screen">Screen Reference</param>
        void RegisterScreen(string name, IScreen screen);
        /// <summary>
        /// Open a Screen
        /// </summary>
        /// <param name="name">Screen Name</param>
        /// <param name="closeCurrentScreen">If we want to close the current screen</param>
        void OpenScreen(string name, bool closeCurrentScreen = true);
        /// <summary>
        /// Open a Screen
        /// </summary>
        /// <param name="screen">Screen Reference</param>
        /// <param name="closeCurrentScreen">If we want to close the current screen</param>
        void OpenScreen(IScreen screen, bool closeCurrentScreen = true);

        /// <summary>
        /// Close the current Screen and don't open the preview one
        /// </summary>
        void CloseCurrentScreen();

        /// <summary>
        /// Close the current screen and open the preview one
        /// </summary>
        void GoToPreviewScreen();

        void GoToPreviewScreen(int screensToClose = 1);
    }
}