﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MagicianGames.Core.ScreenNavigation
{
    public class ScreenNavigation : IScreenNavigation
    {
        private Stack<IScreen> _screens;
        private Dictionary<string, IScreen> _registeredScreens;

        public ScreenNavigation()
        {
            _screens = new Stack<IScreen>();
            _registeredScreens = new Dictionary<string, IScreen>();
        }

        public void RegisterScreen(string name, IScreen screen)
        {
            if (_registeredScreens.ContainsKey(name))
            {
                Debug.LogError("#SCREEN NAVIGATION# Screen " + name + " Already registered as " + _registeredScreens[name].GetType());
                return;
            }

            _registeredScreens.Add(name, screen);
        }

        public void OpenScreen(string name, bool closeCurrentScreen = true)
        {
            if (!_registeredScreens.ContainsKey(name))
            {
                Debug.LogError("#SCREEN NAVIGATION# Screen " + name + " Not Registered");
                return;
            }

            OpenScreen(_registeredScreens[name], closeCurrentScreen);
        }

        public void OpenScreen(IScreen screen, bool closeCurrentScreen = true)
        {
            Debug.Log("#SCREEN NAVIGATION# Open Screen " + screen.GetType().Name);
            screen.Open();

            if (_screens.Count > 0 && closeCurrentScreen)
                _screens.Peek()?.Close();

            _screens.Push(screen);
        }

        public void GoToPreviewScreen()
        {
            if (_screens.Count <= 1)
                return;

            CloseCurrentScreen();

            var screen = _screens.Peek();
            screen.Open();
        }

        public void CloseCurrentScreen()
        {
            if (_screens.Count <= 1)
                return;

            IScreen screen = _screens.Pop();
            screen.Close();
        }

        public void GoToPreviewScreen(int screensToClose = 1)
        {
            IScreen screen = null;

            for (int i = 0; i < screensToClose; i++)
            {
                if (_screens.Count < 2)
                    continue;

                screen = _screens.Pop();
                screen.Close();
            }

            if (_screens.Count > 0)
            {
                screen = _screens.Peek();
                screen.Open();
                Debug.Log("#SCREEN NAVIGATION# Go to Preview Screen " + screen.GetType().Name);
            }
        }

        public override string ToString()
        {
            string s = "#SCREEN NAVIGATION# PEEK SCREEN: " + _screens.Peek().GetType().Name;

            var array = _screens.ToArray();

            foreach (var screen in array)
                s += "\nScreen: " + screen.GetType().Name;

            return s;
        }
    }
}