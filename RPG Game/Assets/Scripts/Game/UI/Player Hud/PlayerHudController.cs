﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MagicianGames.AdventureGame.Game.UI
{
    public class PlayerHudController : MonoBehaviour
    {
        [SerializeField]
        private HudBar _healthBar, _specialBar;

        [Zenject.Inject(Id = "Player")]
        private IHealth _playerHealth;

        private void Start()
        {
            _playerHealth.OnHealthChanged += OnHealthChanged;
            var amount = (float)_playerHealth.CurrentHealth / (float)_playerHealth.MaxHealth;
            _healthBar.Setvalue(amount, false);
        }

        private void OnHealthChanged(int currentHealth, int maxHealth)
        {
            var amount = (float)currentHealth / (float)maxHealth;
            _healthBar.Setvalue(amount);
        }
    }
}