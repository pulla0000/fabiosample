﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

namespace MagicianGames.AdventureGame.Game.UI
{
    public class HudBar : MonoBehaviour
    {
        [SerializeField]
        private Image _foregroundImage;

        public void Setvalue(float amount, bool animated = true)
        {
            float animationSpeed = animated ? .5f : 0;

            _foregroundImage.DOKill();
            _foregroundImage.DOFillAmount(amount, animationSpeed);
        }
    }
}