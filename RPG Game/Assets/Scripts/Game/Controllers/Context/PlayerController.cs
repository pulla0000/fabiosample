﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MagicianGames.AdventureGame.Game.Controllers
{
    public class PlayerController : IPlayerController, Zenject.ITickable
    {
        private const string TAG = "#PLAYER CONTROLLER# ";

        private ICharacterInput _characterInput;
        private IPlayableCharacter _character;

        public bool IsEnabled { get; private set; }
        public IPlayableCharacter GetPlayable() => _character;

        public PlayerController()
        {
            Enable();
        }

        public void Enable()
        {
            IsEnabled = true;
        }

        public void Disable()
        {
            IsEnabled = false;
        }

        [Zenject.Inject]
        public void SetInput(ICharacterInput input)
        {
            _characterInput = input;
            _characterInput.PressedNormalAttack += DoNormalAttack;
            _characterInput.PressedSpecialAttack += DoSpecialAttack;
        }

        [Zenject.Inject]
        public void SetPlayable(IPlayableCharacter playableCharacter)
        {
            _character = playableCharacter;
        }

        private void Update()
        {
            if (!IsEnabled)
                return;

            UpdateCharacterMovement();
        }

        private void UpdateCharacterMovement()
        {
            if (_character == null || _characterInput == null)
                return;

            _character.Move(_characterInput.GetLeftStickDirection());
        }

        private void DoNormalAttack()
        {
            if (!IsEnabled)
                return;

            _character?.DoNormalAttack();
        }

        private void DoSpecialAttack()
        {
            if (!IsEnabled)
                return;

            _character?.DoSpecialAttack();
        }

        public void Tick()
        {
            Update();
        }
    }
}