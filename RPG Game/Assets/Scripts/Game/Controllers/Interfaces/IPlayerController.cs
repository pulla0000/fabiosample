﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MagicianGames.AdventureGame.Game
{
    public interface IPlayerController
    {
        void SetInput(ICharacterInput input);
        void SetPlayable(IPlayableCharacter playableCharacter);
        IPlayableCharacter GetPlayable();
        void Enable();
        void Disable();
    }
}