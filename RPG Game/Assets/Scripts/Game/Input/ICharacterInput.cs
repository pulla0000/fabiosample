﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MagicianGames.AdventureGame.Game
{
    public interface ICharacterInput
    {
        Vector2 GetLeftStickDirection();
        //Vector2 GetRightStickDirection();

        event System.Action InteractionButtonPressed;
        event System.Action PressedSpecialAttack;
        event System.Action PressedNormalAttack;

        void Enable();
        void Disable();
    }
}