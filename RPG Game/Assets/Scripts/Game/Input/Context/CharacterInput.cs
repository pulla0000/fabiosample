﻿using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.DualShock;

namespace MagicianGames.AdventureGame.Game.Input
{
    public class CharacterInput : ICharacterInput
    {
        private readonly PlayerInputMap _inputMap;
        private Vector2 _movementInput;

        public event System.Action PressedSpecialAttack;
        public event System.Action PressedNormalAttack;
        public event System.Action InteractionButtonPressed;

        public CharacterInput()
        {
            _inputMap = new PlayerInputMap();
            _inputMap.PlayerInput.Move.performed += OnMove;
            _inputMap.PlayerInput.Move.canceled += OnMoveCanceled;

            _inputMap.PlayerInput.SpecialAttack.performed += ctx => PressedSpecialAttack?.Invoke();
            _inputMap.PlayerInput.NormalAttack.performed += ctx => PressedNormalAttack?.Invoke();
            _inputMap.PlayerInput.Interaction.performed += ctx => InteractionButtonPressed?.Invoke();

            _inputMap.PlayerInput.Enable();
        }

        #region Movement

        private void OnMoveCanceled(InputAction.CallbackContext ctx)
        {
            _movementInput = Vector2.zero;
        }

        private void OnMove(InputAction.CallbackContext ctx)
        {
            _movementInput = ctx.ReadValue<Vector2>();
        }

        #endregion

        public Vector2 GetLeftStickDirection()
        {
            return _movementInput;
        }

        public void Disable()
        {
            _inputMap.PlayerInput.Disable();
        }

        public void Enable()
        {
            _inputMap.PlayerInput.Enable();
        }
    }
}