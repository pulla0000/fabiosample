﻿using MagicianGames.Core.DialogSystem;
using MagicianGames.Core.QuestSystem;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MagicianGames.AdventureGame.Game.NPC
{
    public class QuestNPC : MonoBehaviour
    {
        [SerializeField]
        private BaseQuest _quest;

        //private BaseQuest _realQuest;
        private IQuest _questManager;

        [Zenject.Inject]
        private IDialogScreen _dialog;

        [Zenject.Inject]
        private IQuestScreen _questScreen;

        [Zenject.Inject]
        private ICharacterInput _input;

        private NPCInteraction _interactionAgent;

        [SerializeField]
        private GameObject _questAvailableIndicator, _questCompletedIndicator;

        private void OnEnable()
        {
            _input.InteractionButtonPressed += OnInteractionPressed;
        }

        private void OnDisable()
        {
            _input.InteractionButtonPressed -= OnInteractionPressed;
        }

        public void OnInteractionPressed()
        {
            var interactable = _quest.State == QuestState.Ready || _quest.State == QuestState.Completed;

            if (_interactionAgent == null || _dialog.IsOpen || !interactable)
                return;

            _input.Disable();

            if (_quest.State == QuestState.Ready)
                ShowAcceptQuestDialog();
            else if (_quest.State == QuestState.Completed)
                ShowQuestComletedDialog();
        }

        #region Accept Quest

        private void ShowAcceptQuestDialog()
        {
            if (_quest.IntroDialog != null)
                _dialog.Show(_quest.IntroDialog, OnIntroDialogCompleted);
            else
                OnIntroDialogCompleted();
        }

        private void OnIntroDialogCompleted()
        {
            _questScreen.Show(_quest, OnQuestScreenClosed);
        }

        private void OnQuestScreenClosed()
        {
            DialogData dialog = null;
            if (_quest.State == QuestState.Started)
                dialog = _quest.AcceptedDialog;
            else
                dialog = _quest.RejectedDialog;

            if (dialog != null)
                _dialog.Show(dialog, OnQuestDialogCompleted);
            else
                OnQuestDialogCompleted();
        }

        #endregion

        #region Quest Completed

        private void ShowQuestComletedDialog()
        {
            if (_quest.CompletedDialog != null)
                _dialog.Show(_quest.CompletedDialog, QuestCompleted);
            else
                QuestCompleted();
        }

        private void QuestCompleted()
        {
            _quest.SetState(QuestState.Done);
            OnQuestDialogCompleted();
        }

        #endregion

        private void OnQuestDialogCompleted()
        {
            _input.Enable();
        }

        [Zenject.Inject]
        public void SetQuestManager(IQuest quest)
        {
            _questManager = quest;
            _quest.OnStateChange += OnQuestStateChaged;

            UpdateStateGraphics(_quest.State);
        }

        private void UpdateStateGraphics(QuestState state)
        {
            _questAvailableIndicator.SetActive(state == QuestState.Ready);
            _questCompletedIndicator.SetActive(state == QuestState.Completed);
        }

        private void OnQuestStateChaged(string id, QuestState state)
        {
            if (id != _quest.Id)
                return;

            UpdateStateGraphics(state);
            /*_questManager.Save();*/
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (_interactionAgent != null)
                return;

            _interactionAgent = collision.gameObject.GetComponent<NPCInteraction>();
        }

        private void OnTriggerExit2D(Collider2D collision)
        {
            if (_interactionAgent == null)
                return;

            if (collision.gameObject == _interactionAgent.gameObject)
                _interactionAgent = null;
        }

        private void OnDestroy()
        {
            _quest.OnStateChange -= OnQuestStateChaged;
            //_realQuest.Dispose();
        }
    }
}