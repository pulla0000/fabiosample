﻿using MagicianGames.Core.DialogSystem;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MagicianGames.AdventureGame.Game.NPC
{
    public class SimpleNPC : MonoBehaviour
    {
        [SerializeField]
        private DialogData _dialogData;

        [Zenject.Inject]
        private IDialogScreen _dialog;

        [Zenject.Inject]
        private ICharacterInput _input;

        private NPCInteraction _interactionAgent;

        private void OnEnable()
        {
            _input.InteractionButtonPressed += OnInteractionPressed;
        }

        private void OnDisable()
        {
            _input.InteractionButtonPressed -= OnInteractionPressed;
        }

        public void OnInteractionPressed()
        {
            if (_interactionAgent == null || _dialog.IsOpen)
                return;

            _dialog.Show(_dialogData,OnDialogCompleted);
            _input.Disable();
        }

        private void OnDialogCompleted()
        {
            _input.Enable();
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (_interactionAgent != null)
                return;

            _interactionAgent = collision.gameObject.GetComponent<NPCInteraction>();
        }

        private void OnTriggerExit2D(Collider2D collision)
        {
            if (_interactionAgent == null)
                return;

            if (collision.gameObject == _interactionAgent.gameObject)
                _interactionAgent = null;
        }
    }
}