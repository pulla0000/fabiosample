﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace MagicianGames.AdventureGame.Game
{
    public class SceneCharactersData : MonoBehaviour
    {
        [SerializeField]
        private CharacterData[] _charactersData;

        public CharacterData? GetCharacter(string id)
        {
            return _charactersData.FirstOrDefault(c => c.Id == id);
        }

        public string GetCharacterName(string id)
        {
            var c = GetCharacter(id);

            if (c != null)
                return c.Value.Name;

            return null;
        }

        public Sprite GetCharacterImage(string id)
        {
            var c = GetCharacter(id);

            if (c != null)
                return c.Value.Image;

            return null;
        }

        [System.Serializable]
        public struct CharacterData
        {
            public string Id, Name;
            public Sprite Image;
        }
    }
}