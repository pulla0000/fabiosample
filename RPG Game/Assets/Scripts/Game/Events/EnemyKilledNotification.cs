﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MagicianGames.AdventureGame
{
    public readonly struct EnemyKilledNotification : INotification
    {
        public string EnemyId { get; }
        public string EnemyClass { get; }

        public EnemyKilledNotification(string id, string enemyClass)
        {
            EnemyId = id;
            EnemyClass = enemyClass;
        }
    }
}