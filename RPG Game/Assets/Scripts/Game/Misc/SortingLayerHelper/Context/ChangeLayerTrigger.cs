﻿using System;
using UnityEngine;

namespace Game.Misc.SortingLayerHelper
{
    [RequireComponent(typeof(Collider2D))]
    public class ChangeLayerTrigger : MonoBehaviour
    {
        [SerializeField]
        private RenderData _changeLayerData;

        private void Awake()
        {
            var colliders = GetComponents<Collider2D>();
            foreach (var collider in colliders)
            {
                collider.isTrigger = true;
            }
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            ISortingLayer otherLayer = other.GetComponent<ISortingLayer>();
            otherLayer?.ChangeLayer(_changeLayerData.SortingLayer, _changeLayerData.OrderInLayer);
        }

        private void OnTriggerExit2D(Collider2D other)
        {
            ISortingLayer otherLayer = other.GetComponent<ISortingLayer>();
            otherLayer?.Reset();
        }
    }
}