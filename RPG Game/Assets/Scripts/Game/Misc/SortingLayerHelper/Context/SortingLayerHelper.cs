﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Misc.SortingLayerHelper
{
    public class SortingLayerHelper : MonoBehaviour, ISortingLayer
    {
        private Dictionary<SpriteRenderer, RenderData> _renderData;

        private void Awake()
        {
            Setup();
        }

        private void Setup()
        {
            _renderData = new Dictionary<SpriteRenderer, RenderData>();
            var renderers = GetComponentsInChildren<SpriteRenderer>();

            foreach (var render in renderers)
            {
                if (!_renderData.ContainsKey(render))
                {
                    _renderData.Add(render, new RenderData());
                }
                
                _renderData[render] = new RenderData()
                {
                    SortingLayer = render.sortingLayerName,
                    OrderInLayer = render.sortingOrder
                };
            }
        }
        
        public void ChangeLayer(string layerName, int order)
        {
            foreach (var kvp in _renderData)
            {
                kvp.Key.sortingLayerName = layerName;
                kvp.Key.sortingOrder = kvp.Value.OrderInLayer + order;
            }
        }

        public void Reset()
        {
            foreach (var kvp in _renderData)
            {
                kvp.Key.sortingLayerName = kvp.Value.SortingLayer;
                kvp.Key.sortingOrder = kvp.Value.OrderInLayer;
            }
        }
    }
}