﻿namespace Game.Misc.SortingLayerHelper
{
    public interface ISortingLayer
    {
        void ChangeLayer(string layerName, int order);
        void Reset();
    }
    
    [System.Serializable]
    public struct RenderData
    {
        public string SortingLayer;
        public int OrderInLayer;
    }
}