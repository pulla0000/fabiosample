﻿using Core.InputSystem.LightPatterns;

namespace Game.Misc
{
    public interface IGamepadLightController
    {
        void SetDefaultLight(ILightPattern pattern);
        void ShowLight(ILightPattern pattern);
    }
}