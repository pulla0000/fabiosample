﻿using System;
using Core.InputSystem;
using Core.InputSystem.Gamepads;
using Core.InputSystem.LightPatterns;
using UnityEngine;

namespace Game.Misc
{
    public class GamepadLightController:MonoBehaviour, IGamepadLightController
    {
        [SerializeField]
        private Color _defaultColor = Color.white;
        
        private ILightPattern _defaultLight;
        private IInputSystem _inputSystem;

        private void Start()
        {
            SetDefaultLight(new GamepadStaticLightPattern(_defaultColor));
        }

        [Zenject.Inject]
        private void RegisterInput(IInputSystem inputSystem)
        {
            _inputSystem = inputSystem;
        }

        public void SetDefaultLight(ILightPattern pattern)
        {
            _defaultLight = pattern;
            _inputSystem.GetGamepad()?.ChangeLight(_defaultLight);
        }

        public void ShowLight(ILightPattern pattern)
        {
            if (pattern == null)
            {
                return;
            }

            _inputSystem.GetGamepad()?.ChangeLight(_defaultLight, OnLightCompleted);
        }

        private void OnLightCompleted()
        {
            _inputSystem.GetGamepad()?.ChangeLight(_defaultLight);
        }
    }
}