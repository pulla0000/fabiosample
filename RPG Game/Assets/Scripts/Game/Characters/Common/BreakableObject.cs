﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MagicianGames.AdventureGame.Game.Common
{
    public class BreakableObject : MonoBehaviour, IKillable
    {
        [SerializeField]
        private int _health = 10;

        public event System.Action OnDie;

        public void TakeDamage(int damage)
        {
            _health -= damage;

            if (_health < 1)
                Destroy(gameObject);
        }
    }
}