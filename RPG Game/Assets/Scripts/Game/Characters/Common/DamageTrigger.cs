﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MagicianGames.AdventureGame.Game
{
    public class DamageTrigger : MonoBehaviour
    {
        [SerializeField]
        private int _damage = -1;

        private void Awake()
        {
            var collider = GetComponent<Collider2D>();

            if (collider == null)
            {
                Debug.LogError(gameObject.name + " does not have a 2D collider", gameObject);
                return;
            }

            collider.isTrigger = true;
        }

        public void SetDamage(int damage)
        {
            _damage = damage;
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (_damage < 1)
                return;

            var killable = other.gameObject.GetComponent<IKillable>();
            killable?.TakeDamage(_damage);
        }
    }
}