﻿using Game.Misc;
using UnityEngine;

namespace MagicianGames.AdventureGame.Game.Common
{
    public class DemoEnemy : MonoBehaviour, IKillable
    {
        [SerializeField]
        private string _id;

        [SerializeField]
        private int _health = 10;

        public event System.Action OnDie;

        [Zenject.Inject]
        private IGamepadLightController _gamepadLightController;
        
        public void TakeDamage(int damage)
        {
            _health -= damage;

            if (_health < 1)
                Kill();
        }

        private void Kill()
        {
            NotificationsManager.Raise(new EnemyKilledNotification(_id, string.Empty));
            Destroy(gameObject);
        }
    }
}