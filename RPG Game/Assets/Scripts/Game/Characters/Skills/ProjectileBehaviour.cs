﻿using System.Collections;
using System.Collections.Generic;
using Core.InputSystem;
using Core.InputSystem.RumblePatterns;
using UnityEngine;

namespace MagicianGames.AdventureGame.Game.Skills
{
    [RequireComponent(typeof(Rigidbody2D))]
    public class ProjectileBehaviour : MonoBehaviour
    {
        [SerializeField]
        private Transform _impactParticle;

        [SerializeField]
        private float _projectileSpeed = 50;

        private Rigidbody2D _rigidbody;
        private Collider2D _collider;

        [Zenject.Inject]
        private IInputSystem _inputSystem;

        private void Awake()
        {
            _rigidbody = GetComponent<Rigidbody2D>();
            _collider = GetComponent<Collider2D>();

            _collider.isTrigger = true;
            Destroy(gameObject, 8);
        }

        public void Launch(Vector2 direction)
        {
            _rigidbody.AddForce(direction * _projectileSpeed, ForceMode2D.Force);
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (_impactParticle != null)
            {
                var impact = Instantiate(_impactParticle, transform.position, Quaternion.identity).gameObject;
                Destroy(impact, 5);
            }
            _inputSystem.GetGamepad()?.StartRumble(new SimpleRumble(.5f,.15f));
            /*_collider.enabled = false;*/
            Destroy(gameObject);
        }
    }
}