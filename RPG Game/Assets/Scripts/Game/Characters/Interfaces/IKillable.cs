﻿namespace MagicianGames.AdventureGame.Game
{
    public interface IKillable
    {
        event System.Action OnDie;
        void TakeDamage(int damage);
    }
}