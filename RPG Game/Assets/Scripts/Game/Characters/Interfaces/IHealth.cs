﻿namespace MagicianGames.AdventureGame.Game
{
    public delegate void HealthChangeEvent(int currentHealth, int maxHealth);
    public interface IHealth
    {
        event HealthChangeEvent OnHealthChanged;
        event System.Action<int> OnMaxHealthChanged;

        int CurrentHealth { get; }
        int MaxHealth { get; }
        void IncreaseHealth(int val);
        void DecreaseHealth(int val);
        void IncreaseMaxHealth(int val);
        void DecreaseMaxHealth(int val);
    }
}