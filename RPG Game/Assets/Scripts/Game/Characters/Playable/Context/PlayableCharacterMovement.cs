﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MagicianGames.AdventureGame.Game.Characters.Playable
{
    public class PlayableCharacterMovement
    {
        private Rigidbody2D _rigidbody;
        private float _movementSpeed;

        public float CurrentSpeed => _rigidbody.velocity.sqrMagnitude;

        public PlayableCharacterMovement(Rigidbody2D rigidbody, float movementSpeed)
        {
            _rigidbody = rigidbody;
            _movementSpeed = movementSpeed;
        }

        public void Move(Vector2 direction)
        {
            _rigidbody.velocity = direction * _movementSpeed;
        }

        public void Stop()
        {
            _rigidbody.velocity = Vector2.zero;
        }
    }
}