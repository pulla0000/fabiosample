﻿using MagicianGames.AdventureGame.Game.Skills;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MagicianGames.AdventureGame.Game.Characters.Playable
{
    public class PlayableCharacter : IPlayableCharacter
    {
        private const float INPUT_DEAD_ZONE = .5f;

        private Transform _transform, _projectileSpawnPoint;
        private ProjectileBehaviour _projectilePrefab;
        private Animator _animator;
        private PlayableCharacterMovement _movement;
        private Vector2 _lastDirection;

        private float _lastAttackTime = 0;
        private float _attackCoolDown = 1;

        public bool CanAttack => Time.time - _lastAttackTime >= _attackCoolDown;

        public PlayableCharacter(Transform transform, Transform projectileSpawnPoint, ProjectileBehaviour projectilePrefab/*, CHARACTER ATTRIBUTES */)//TODO REPLACE THESE PARAMETERS
        {
            _transform = transform;
            _animator = _transform.GetComponent<Animator>();
            _projectilePrefab = projectilePrefab;
            _projectileSpawnPoint = projectileSpawnPoint;
            _movement = new PlayableCharacterMovement(transform.GetComponent<Rigidbody2D>(), 5);//TODO GET SPEED VALUE
        }

        public Transform GetTransform()
        {
            return _transform;
        }

        #region Movement

        public void Move(Vector2 direction)
        {            
            var dir = ClampDirection(direction);
            CheckFlip(dir.x);

            _movement.Move(dir);

            bool isMoving = _movement.CurrentSpeed > 0;

            _animator.SetBool("IsMoving", isMoving);

            if (isMoving)
                _lastDirection = dir;

            var h = _lastDirection.x;

            if (h < 0)
                h *= -1;

            _animator.SetFloat("Horizontal", h);
            _animator.SetFloat("Vertical", _lastDirection.y);
        }

        private void CheckFlip(float dir)
        {
            var scale = _transform.localScale;
            if (dir < 0 && scale.x > 0)
            {
                scale.x = -1;
                _transform.localScale = scale;
            }

            if (dir > 0 && scale.x < 0)
            {
                scale.x = 1;
                _transform.localScale = scale;
            }
        }

        public void Stop()
        {
            _movement.Stop();
            _animator.SetBool("IsMoving", false);
        }

        private Vector2 ClampDirection(Vector2 direction)
        {
            Vector2 dir = direction;

            if (dir.x > INPUT_DEAD_ZONE)
                dir.x = 1;
            else if (dir.x < -INPUT_DEAD_ZONE)
                dir.x = -1;
            else
                dir.x = 0;

            if (dir.y > INPUT_DEAD_ZONE)
                dir.y = 1;
            else if (dir.y < -INPUT_DEAD_ZONE)
                dir.y = -1;
            else
                dir.y = 0;

            return dir;
        }

        #endregion

        public void Die()
        {
            Stop();
            _animator.SetTrigger("Die");
        }

        #region Attack

        public void DoNormalAttack()
        {

        }

        public void DoSpecialAttack()
        {
            if (!CanAttack)
                return;

            _lastAttackTime = Time.time;

            var projectile = MonoBehaviour.Instantiate(_projectilePrefab, _projectileSpawnPoint.position, _projectileSpawnPoint.rotation);
            projectile.Launch(_projectileSpawnPoint.up);
        }

        #endregion
    }
}