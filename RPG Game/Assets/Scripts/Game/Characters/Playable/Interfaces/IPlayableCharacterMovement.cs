﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MagicianGames.AdventureGame.Game
{
    public interface IPlayableCharacterMovement
    {
        void Move(Vector2 direction);
        void Stop();
    }
}