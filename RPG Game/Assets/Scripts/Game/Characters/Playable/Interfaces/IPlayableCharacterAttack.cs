﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MagicianGames.AdventureGame.Game
{
    public interface IPlayableCharacterAttack
    {
        void DoNormalAttack();
        void DoSpecialAttack();
    }
}