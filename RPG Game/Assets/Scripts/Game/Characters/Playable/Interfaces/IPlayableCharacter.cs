﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MagicianGames.AdventureGame.Game
{
    public interface IPlayableCharacter : IPlayableCharacterMovement, IPlayableCharacterAttack
    {
        Transform GetTransform();
        void Die();
    }
}