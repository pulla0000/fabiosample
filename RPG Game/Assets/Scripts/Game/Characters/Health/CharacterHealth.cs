﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MagicianGames.AdventureGame.Game.Health
{
    public class CharacterHealth : MonoBehaviour, IKillable, IHealth
    {
        public int CurrentHealth { get; private set; }
        public int MaxHealth { get; private set; }

        public event HealthChangeEvent OnHealthChanged;
        public event System.Action<int> OnMaxHealthChanged;
        public event System.Action OnDie;

        public void Setup(int maxHealth)
        {
            MaxHealth = maxHealth;
            CurrentHealth = maxHealth;
        }

        public void IncreaseHealth(int val)
        {
            CurrentHealth += val;
            ClampHealth();
            OnHealthChanged?.Invoke(CurrentHealth, MaxHealth);
        }

        public void DecreaseHealth(int val)
        {
            CurrentHealth -= val;
            ClampHealth();
            OnHealthChanged?.Invoke(CurrentHealth, MaxHealth);
        }

        private void ClampHealth()
        {
            CurrentHealth = Mathf.Clamp(CurrentHealth, 0, MaxHealth);
        }

        public void IncreaseMaxHealth(int val)
        {
            MaxHealth += val;
            OnMaxHealthChanged?.Invoke(MaxHealth);
        }

        public void DecreaseMaxHealth(int val)
        {
            MaxHealth -= val;
            OnMaxHealthChanged?.Invoke(MaxHealth);
        }

        public void TakeDamage(int damage)
        {
            if (CurrentHealth < 1)
                return;

            DecreaseHealth(damage);

            if (CurrentHealth < 1)
            {
                OnDie?.Invoke();
            }
        }
    }
}