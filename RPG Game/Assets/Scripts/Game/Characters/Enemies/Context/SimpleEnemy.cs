﻿using Core.InputSystem;
using Core.InputSystem.LightPatterns;
using Game.Characters.Enemies;
using Game.Misc;
using MagicianGames.AdventureGame.Game.Health;
using UnityEngine;

namespace Game.Characters.Enemies
{
    public class SimpleEnemy : MonoBehaviour, IEnemy
    {
        private CharacterHealth _health;

        private ILightPattern _diePattern;
        
        [Zenject.Inject]
        private IGamepadLightController _lightController;
        
        private void Awake()
        {
            _diePattern = new GamepadBlinkLightPattern(Color.red, Color.black, .15f, 3);
            
            _health = GetComponent<CharacterHealth>();
            _health.Setup(10);
            _health.OnDie +=OnDie;
        }

        private void OnDie()
        {
            _lightController.ShowLight(_diePattern);
        }
    }
}