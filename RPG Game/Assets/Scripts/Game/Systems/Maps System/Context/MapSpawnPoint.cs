﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace MagicianGames.AdventureGame.Game.Systems.MapSystem
{
    public class MapSpawnPoint : MonoBehaviour
    {
        [SerializeField]
        private string _id;

        public string Id => _id;
    }
}