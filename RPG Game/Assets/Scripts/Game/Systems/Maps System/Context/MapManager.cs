﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MagicianGames.AdventureGame.Game.Systems.MapSystem
{
    public class MapManager : IMapManager
    {
        private string _spawnPointId;

        public string GetSpawnPoint()
        {
            return _spawnPointId;
        }

        public void SetSpawnPoint(string id)
        {
            _spawnPointId = id;
        }
    }
}