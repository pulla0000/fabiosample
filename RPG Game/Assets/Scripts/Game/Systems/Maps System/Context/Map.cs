﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace MagicianGames.AdventureGame.Game.Systems.MapSystem
{
    public class Map : MonoBehaviour, IMap
    {
        [SerializeField]
        private Transform _defaultSpawnPoint;

        [Zenject.Inject]
        private IMapManager _mapManager;

        [Zenject.Inject]
        private IPlayableCharacter _character;

        [Zenject.Inject]
        private IGameData _gameData;
        
        // Start is called before the first frame update
        void Start()
        {
            var spawnPointId = _mapManager.GetSpawnPoint();
            var pos = _defaultSpawnPoint.position;

            if (!string.IsNullOrEmpty(spawnPointId))
                pos = GetSpawnPointPoint(spawnPointId);

            var currentScene = UnityEngine.SceneManagement.SceneManager.GetActiveScene().name;

            if (_gameData.GameData.CurrentScene != currentScene)
            {
                _gameData.GameData.CurrentScene = currentScene;
                _gameData.Save();
            }

            _character.GetTransform().position = pos;
            _mapManager.SetSpawnPoint(null);
            StartCoroutine(UpdateGameTime());
        }

        public Vector3 GetSpawnPointPoint(string id)
        {
            var spawPoints = GetComponentsInChildren<MapSpawnPoint>(true);
            var spawPoint = spawPoints.FirstOrDefault(s => s.Id == id);

            if (spawPoint == null)
                Debug.LogError("Could not find spawn point with id: " + id);
            else
                return spawPoint.transform.position;

            return Vector3.zero;
        }

        private IEnumerator UpdateGameTime()
        {
            while (true)
            {
                yield return 0;
                _gameData.AddGameTime(Time.unscaledDeltaTime);
            }
        }
    }
}