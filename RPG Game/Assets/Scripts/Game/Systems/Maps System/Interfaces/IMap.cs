﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MagicianGames.AdventureGame.Game.Systems.MapSystem
{
    public interface IMap
    {
        Vector3 GetSpawnPointPoint(string id);
    }
}