﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MagicianGames.AdventureGame.Game
{
    public interface IMapManager
    {
        string GetSpawnPoint();
        void SetSpawnPoint(string id);
    }
}