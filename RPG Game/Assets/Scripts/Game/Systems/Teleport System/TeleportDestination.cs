﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MagicianGames.AdventureGame.Game.Systems.Teleport
{
    public class TeleportDestination : MonoBehaviour
    {
        [SerializeField]
        private GameObject _inside;

        [SerializeField]
        private Transform _entryPoint;

        //public Vector3 EntryPoint => _entryPoint.position;

        private void Start()
        {
            if (_inside != null)
                _inside.SetActive(false);
        }

        public void Entry(TeleportAgent agent)
        {
            if (_inside != null)
                _inside.SetActive(true);

            agent.transform.position = _entryPoint.position;
        }

        public void Leave()
        {
            if (_inside != null)
                _inside.SetActive(false);
        }
    }
}