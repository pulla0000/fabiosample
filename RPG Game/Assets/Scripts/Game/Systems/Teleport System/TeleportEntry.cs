﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MagicianGames.AdventureGame.Game.Systems.Teleport
{
    public class TeleportEntry : TeleportZone
    {
        protected override void OnFadeIn(TeleportAgent agent)
        {
            _destination.Entry(agent);
            _fadeScreen.FadeOut(.75f, () =>
            {
                OnFadeOut(agent);
            });
        }

        protected override void OnFadeOut(TeleportAgent agent)
        {
            _fadeScreen.Close();
            _controller.Enable();
        }
    }
}