﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MagicianGames.AdventureGame.Game.Systems.Teleport
{
    public abstract class TeleportZone : MonoBehaviour
    {
        [SerializeField]
        protected TeleportDestination _destination;

        [Zenject.Inject]
        protected IFadeScreen _fadeScreen;

        [Zenject.Inject]
        protected IPlayerController _controller;

        private void OnTriggerEnter2D(Collider2D collision)
        {
            var agent = collision.gameObject.GetComponent<TeleportAgent>();

            if (agent == null)
                return;

            Teleport(agent);
        }

        private void Teleport(TeleportAgent agent)
        {
            _controller.Disable();
            _controller.GetPlayable().Stop();
            _fadeScreen.FadeIn(
                () =>
                {
                    OnFadeIn(agent);
                });
        }

        protected abstract void OnFadeIn(TeleportAgent agent);
        protected abstract void OnFadeOut(TeleportAgent agent);
    }
}