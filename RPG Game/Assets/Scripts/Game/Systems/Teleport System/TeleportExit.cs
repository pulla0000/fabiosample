﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MagicianGames.AdventureGame.Game.Systems.Teleport
{
    public class TeleportExit : TeleportZone
    {
        private TeleportDestination _origin;

        private void Start()
        {
            _origin = GetComponentInParent<TeleportDestination>();
        }

        protected override void OnFadeIn(TeleportAgent agent)
        {
            _destination.Entry(agent);
            _origin.Leave();
            _fadeScreen.FadeOut(.75f, () =>
            {
                OnFadeOut(agent);
            });
        }

        protected override void OnFadeOut(TeleportAgent agent)
        {
            _fadeScreen.Close();
            _controller.Enable();
        }
    }
}