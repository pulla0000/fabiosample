﻿using MagicianGames.AdventureGame.Game.Systems.Teleport;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using UnityEngine;

namespace MagicianGames.AdventureGame.Game.Systems
{
    public class ChangeMapTrigger : MonoBehaviour
    {
        [SerializeField]
        private bool _saveOnChangeMap = true;
        
        [SerializeField]
        private LoadMapData _mapToLoad;

        [Zenject.Inject]
        private IPlayerController _playerController;

        [Zenject.Inject]
        private IScenesLoader _scenesLoader;

        [Zenject.Inject]
        private IMapManager _mapManager;

        [Zenject.Inject] 
        private IGameData _gameData;
        
        private Collider2D _collider;

        private void Awake()
        {
            _collider = GetComponent<Collider2D>();

            if (_collider == null)
                Debug.LogError(gameObject.name + " does not have a collider", gameObject);

            _collider.isTrigger = true;
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            //Debug.Log(gameObject.name + " OnTriggerEnter2D(), other: " + collision.gameObject.name, collision.gameObject);
            var playable = collision.gameObject.GetComponent<TeleportAgent>();

            if (playable == null)
                return;

            _playerController.Disable();
            _playerController.GetPlayable().Stop();

            if (_saveOnChangeMap)
            {
                _gameData.Save();
            }
            
            _mapManager.SetSpawnPoint(_mapToLoad.SpawnPointId);
            _scenesLoader.LoadScene(_mapToLoad.MapName, false);
        }

        [System.Serializable]
        private struct LoadMapData
        {
            public string MapName, SpawnPointId;
        }
    }
}