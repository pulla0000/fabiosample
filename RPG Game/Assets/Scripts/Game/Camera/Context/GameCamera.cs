﻿using System;
using Cinemachine;
using Game.Camera.Interfaces;
using UnityEngine;

namespace Game.Camera
{
    public class GameCamera : MonoBehaviour, IGameCamera
    {
        private CinemachineBrain _cinemachineBrain;

        private void Awake()
        {
            _cinemachineBrain = GetComponent<CinemachineBrain>();
        }

        public void ChangeBlendMode(CinemachineBlendDefinition blendMode)
        {
            _cinemachineBrain.m_DefaultBlend = blendMode;
        }
    }
}