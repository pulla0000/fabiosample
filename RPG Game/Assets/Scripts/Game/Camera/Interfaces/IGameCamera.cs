﻿using Cinemachine;

namespace Game.Camera.Interfaces
{
    public interface IGameCamera
    {
        void ChangeBlendMode(CinemachineBlendDefinition blendMode);
    }
}