﻿using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.EventSystems;

namespace MagicianGames.AdventureGame.TitleScreen
{
    public class CreditsScreenController : MonoBehaviour, IScreen, ICancelHandler
    {
        [SerializeField]
        private GameObject _window;

        [SerializeField]
        private GameObject _defaultSelection;

        private IScreenNavigation _screenNavigation;
        
        public bool IsOpen => _window.activeSelf;

        private void Awake()
        {
            Close();
        }

        [UsedImplicitly]
        [Zenject.Inject]
        private void Register(IScreenNavigation navigation)
        {
            _screenNavigation = navigation;
            _screenNavigation.RegisterScreen(ScreenName.CREDITS_SCREEN, this);
        }

        public void Open()
        {
            if (IsOpen)
                return;

            _window.SetActive(true);
            EventSystem.current.SetSelectedGameObject(_defaultSelection);
        }

        public void Close()
        {
            _window.SetActive(false);
        }

        public void OnCancel(BaseEventData eventData)
        {
            _screenNavigation.GoToPreviewScreen();
        }
    }
}