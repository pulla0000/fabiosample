﻿using System.Linq;
using Core.Environment;
using Core.Ui;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;
using UnityEngine.UI;

namespace MagicianGames.AdventureGame.TitleScreen.OptionsScreen
{
    public class OptionsScreenController : MonoBehaviour, IScreen, ICancelHandler
    {
        [SerializeField] private GameObject _window, _applyChangesButton;

        private IOptionsTab _currentOptionsTab;

        [Zenject.Inject] private IConfirmationPopup _confirmationPopup;

        [Zenject.Inject] private ISettingsData _settings;

        [SerializeField] private TabOption[] _tabsToggle = default;

        private int _currentTabIndex;
        private InputActionHandler _inputActionHandler;
        
        private IScreenNavigation _screenNavigation;

        public bool IsOpen => _window.activeSelf;

        // Start is called before the first frame update
        void Start()
        {
            _inputActionHandler = GetComponent<InputActionHandler>();
            SetupTabs();
            Close();
        }

        [Zenject.Inject]
        private void Register(IScreenNavigation navigation)
        {
            _screenNavigation = navigation;
            _screenNavigation.RegisterScreen(ScreenName.OPTIONS_SCREEN, this);
        }

        private void EnableInput()
        {
            _inputActionHandler.EnableInput();
        }

        private void DisableInput()
        {
            _inputActionHandler.DisableInput();
        }

        private void SetupTabs()
        {
            for (int i = 0; i < _tabsToggle.Length; i++)
            {
                int index = i;
                var tab = _tabsToggle[i];

                tab.Toggle.onValueChanged.AddListener(
                    (isOn) =>
                    {
                        if (isOn)
                        {
                            OpenTab(tab.Tab);
                        }
                    });
            }
        }

        private void ResetTabs()
        {
            foreach (var tab in _tabsToggle)
            {
                tab.Toggle.SetIsOnWithoutNotify(false);
            }
        }

        public void Open()
        {
            foreach (var tab in _tabsToggle)
            {
                tab.Tab.Setup();
            }

            _applyChangesButton.SetActive(false);
            _window.SetActive(true);
            _currentTabIndex = 0;
            OpenCurrentState();

            EnableInput();
        }

        public void Close()
        {
            _window.SetActive(false);
            ResetTabs();
            DisableInput();
        }

        public void ApplyChanges()
        {
            /*_currentOptionsTab?.ApplyChanges();*/

            foreach (var tab in _tabsToggle)
            {
                tab.Tab.ApplyChanges();
            }

            _applyChangesButton.SetActive(false);
            _settings.Save();
            ApplyResolution();
        }

        private void ApplyResolution()
        {
            if (EnvironmentVariables.IsEditor)
            {
                return;
            }

            if (Screen.currentResolution.IsSameResolutionAs(_settings.Settings.Resolution))
            {
                if (_settings.Settings.FullScreen != Screen.fullScreen)
                    Screen.fullScreen = _settings.Settings.FullScreen;
            }
            else
            {
                Screen.SetResolution(_settings.Settings.Resolution.Width, _settings.Settings.Resolution.Height,
                    _settings.Settings.FullScreen);
            }
        }

        private void OpenCurrentState()
        {
            _tabsToggle[_currentTabIndex].Toggle.isOn = true;
        }

        [UsedImplicitly]
        public void GoToPreviousTab()
        {
            if (!IsOpen)
            {
                return;
            }

            ChangeTab(-1);
        }

        [UsedImplicitly]
        public void GoToNextTab()
        {
            if (!IsOpen)
            {
                return;
            }

            ChangeTab(1);
        }

        [UsedImplicitly]
        public void ApplyChangesPressed()
        {
            if (!_applyChangesButton.activeSelf)
            {
                return;
            }
            
            ApplyChanges();
        }
        
        private void ChangeTab(int value)
        {
            if (!ChangeTabIndex(value, out var newIndex))
            {
                return;
            }

            if (!CanChangeTab())
            {
                return;
            }

            _currentTabIndex = newIndex;
            OpenCurrentState();
        }

        private bool ChangeTabIndex(int value, out int newIndex)
        {
            newIndex = Mathf.Clamp(_currentTabIndex + value, 0, _tabsToggle.Length - 1);
            return _currentTabIndex != newIndex;
        }

        private bool CanChangeTab()
        {
            /*if (_currentOptionsTab.HasChanges)
            {
                _twoButtonPopup.Open("You have some changes", "Discard", "Keep",
                    buttonCallback: (int button) =>
                    {
                        switch (button)
                        {
                            //DISCARD
                            case 1:
                                CloseCurrentTab();
                                OpenTab(tab);
                                break;
                        }
                    });
                
                return false;
            }*/

            return true;
        }

        #region Buttons Callback

        private void OpenTab(IOptionsTab tab)
        {
            CloseCurrentTab();

            _currentOptionsTab = tab;
            _currentOptionsTab.Open();
            _currentOptionsTab.OnValueChanged += OnOptionFieldChanged;
            /*_applyChangesButton.SetActive(false);*/
        }

        private void CloseCurrentTab()
        {
            if (_currentOptionsTab != null)
            {
                _currentOptionsTab.OnValueChanged -= OnOptionFieldChanged;
                _currentOptionsTab.Close();
                _currentOptionsTab = null;
            }
        }

        #endregion

        private void OnOptionFieldChanged()
        {
            _applyChangesButton.SetActive(HasChanges());
        }

        private bool HasChanges()
        {
            return _tabsToggle.Any(t => t.Tab.HasChanges);
        }

        public void OnCancel(BaseEventData eventData)
        {
            DisableInput();
            
            if (!HasChanges())
            {
                _screenNavigation.GoToPreviewScreen();
                return;
            }

            _confirmationPopup.Open("You have some changes",
                buttonCallback: (int button) =>
                {
                    switch (button)
                    {
                        case 0:
                            EnableInput();
                            break;
                        case 1:
                            _screenNavigation.GoToPreviewScreen();
                            break;
                        case 2:
                            ApplyChanges();
                            _screenNavigation.GoToPreviewScreen();
                            break;
                    }
                }, "Cancel", "Discard", "Apply");
        }

        [System.Serializable]
        private struct TabOption
        {
            public Toggle Toggle;
            public GameObject TabWindow;

            private IOptionsTab _tab;

            public IOptionsTab Tab
            {
                get
                {
                    if (_tab == null)
                    {
                        _tab = TabWindow.GetComponent<IOptionsTab>();

                        if (_tab == null)
                        {
                            Debug.LogError(
                                "Game Object " + TabWindow.name + " does not have a script who implements IOptionsTab",
                                TabWindow);
                        }
                    }

                    return _tab;
                }
            }
        }
    }
}