﻿namespace MagicianGames.AdventureGame.TitleScreen.OptionsScreen
{
    public interface IOptionsTab
    {
        event System.Action OnValueChanged;
        bool HasChanges { get; }
        void Setup();
        void Open();
        void Close();
        void ApplyChanges();
    }
}