﻿namespace MagicianGames.AdventureGame.TitleScreen.OptionsScreen
{
    public interface IOptionsField
    {
        event System.Action OnValueChanged;
        bool HasChange { get; }
        void Setup();
        void ApplyChange();
    }
}