﻿using UnityEngine;
using System.Linq;
using System;
using UnityEngine.EventSystems;

namespace MagicianGames.AdventureGame.TitleScreen.OptionsScreen
{
    public class OptionsTab : MonoBehaviour, IOptionsTab
    {
        [SerializeField]
        private GameObject _defaultSelection = default;

        private IOptionsField[] _fields;

        public event Action OnValueChanged;

        public bool HasChanges
        {
            get
            {
                /*Setup();*/
                return _fields.Any(f => f.HasChange);
            }
        }

        public void Setup()
        {
            if (_fields == null)
            {
                _fields = GetComponentsInChildren<IOptionsField>();
                foreach (var f in _fields)
                {
                    f.OnValueChanged +=
                        () =>
                        {
                            OnValueChanged?.Invoke();
                        };
                }
            }
            
            foreach (var f in _fields)
            {
                f.Setup();
            }
        }

        public void Open()
        {
            gameObject.SetActive(true);
            EventSystem.current.SetSelectedGameObject(_defaultSelection);
        }

        public void Close()
        {
            gameObject.SetActive(false);
        }

        public void ApplyChanges()
        {
            /*Setup();*/
            foreach (var f in _fields)
            {
                f.ApplyChange();
            }
        }
    }
}