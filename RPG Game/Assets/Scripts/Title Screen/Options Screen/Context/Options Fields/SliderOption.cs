﻿using UnityEngine;
using UnityEngine.UI;

namespace MagicianGames.AdventureGame.TitleScreen.OptionsScreen
{
    public abstract class SliderOption : MonoBehaviour, IOptionsField
    {
        [SerializeField]
        protected Slider _slider;

        protected float _initialValue;

        public bool HasChange => _slider.value != _initialValue;

        public event System.Action OnValueChanged;

        // Start is called before the first frame update
        protected virtual void Start()
        {

        }

        public virtual void Setup()
        {
            _slider.onValueChanged.AddListener(OnSliderValueChanged);
        }

        public virtual void ApplyChange()
        {
            _initialValue = _slider.value;
        }

        private void OnSliderValueChanged(float val)
        {
            OnValueChanged?.Invoke();
        }

        private void OnDisable()
        {
            _slider.onValueChanged.RemoveAllListeners();
        }
    }
}