﻿using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace MagicianGames.AdventureGame.TitleScreen.OptionsScreen
{
    public abstract class DropdownOption : MonoBehaviour, IOptionsField
    {
        [SerializeField] protected TMP_Dropdown _dropdown = default;

        protected int _initialValue, _currentValue;

        public virtual bool HasChange => _initialValue != _currentValue;

        public abstract event System.Action OnValueChanged;

        protected abstract void OnSelect();

        public virtual void Setup()
        {
            if (_dropdown == null)
            {
                Debug.LogError(gameObject.name + " doesn't have a dropdown", gameObject);
            }

            _dropdown.onValueChanged.AddListener(OnValueSelected);
        }

        protected abstract void OnValueSelected(int value);
        protected abstract List<string> GetOptions();

        protected virtual void PopulateDropdown()
        {
            _dropdown.options.Clear();
            _dropdown.AddOptions(GetOptions());
        }

        public abstract void ApplyChange();
    }
}