﻿namespace MagicianGames.AdventureGame.TitleScreen.OptionsScreen
{
    public class MusicOptionSlider : SliderOption
    {
        [Zenject.Inject]
        private ISettingsData _settings;

        public override void Setup()
        {
            _slider.minValue = -80;
            _slider.maxValue = 0;

            _initialValue = _settings.Settings.MusicVolume;
            _slider.value = _initialValue;

            base.Setup();
        }

        public override void ApplyChange()
        {
            base.ApplyChange();
            _settings.Settings.MusicVolume = (int)_slider.value;
        }
    }
}