﻿namespace MagicianGames.AdventureGame.TitleScreen.OptionsScreen
{
    public class SfxOptionSlider : SliderOption
    {
        [Zenject.Inject]
        private ISettingsData _settings;

        public override void Setup()
        {
            _slider.minValue = -80;
            _slider.maxValue = 0;

            _initialValue = _settings.Settings.SfxVolume;
            _slider.value = _initialValue;

            base.Setup();
        }

        public override void ApplyChange()
        {
            base.ApplyChange();
            _settings.Settings.SfxVolume = (int)_slider.value;
        }
    }
}