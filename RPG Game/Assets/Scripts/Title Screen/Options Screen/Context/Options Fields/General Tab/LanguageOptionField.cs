﻿using System.Collections.Generic;
using System.Linq;

namespace MagicianGames.AdventureGame.TitleScreen.OptionsScreen
{
    public class LanguageOptionField : DropdownOption
    {
        private readonly string[] TEST = { "English", "Portuguese", "Spanish" };

        [Zenject.Inject]
        private ISettingsData _settings;

        private bool _isInitialized = false;
        
        public override event System.Action OnValueChanged;

        public override void Setup()
        {
            if (!_isInitialized)
            {
                PopulateDropdown();
                _dropdown.SetValueWithoutNotify(_currentValue);
                base.Setup();
                _isInitialized = true;
            }
            
            _initialValue = _settings.Settings.Language;
            _currentValue = _initialValue;
        }

        public override void ApplyChange()
        {
            _settings.Settings.Language = _currentValue;
            _initialValue = _currentValue;
        }

        protected override void OnSelect()
        {
        }

        protected override List<string> GetOptions()
        {
            return TEST.ToList();
        }
        
        protected override void OnValueSelected(int value)
        {
            if (value == _currentValue)
                return;

            _currentValue = value;
            OnValueChanged?.Invoke();
        }
    }
}