﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MagicianGames.AdventureGame.TitleScreen.OptionsScreen
{
    public class WindowModeOptionField : DropdownOption
    {
        private readonly string[] TEST = { "Fullscreen", "Windowed" };

        /*[Zenject.Inject]
        private IOptionsDisplay _optionsDisplay;*/

        private bool _isInitialized;
        
        [Zenject.Inject]
        private ISettingsData _settings;

        public override event Action OnValueChanged;

        public override void Setup()
        {
            if (!_isInitialized)
            {
                base.Setup();
                PopulateDropdown();
                _isInitialized = true;
            }
            
            _initialValue = _settings.Settings.FullScreen ? 0 : 1;
            _currentValue = _initialValue;
            _dropdown.SetValueWithoutNotify(_currentValue);
        }

        protected override void OnValueSelected(int value)
        {
            var res = value;

            if (res == _currentValue)
                return;

            _currentValue = res;
            OnValueChanged?.Invoke();
        }

        protected override List<string> GetOptions()
        {
            return TEST.ToList();
        }

        public override void ApplyChange()
        {
            _initialValue = _currentValue;
            _settings.Settings.FullScreen = _currentValue == 0;
        }

        protected override void OnSelect()
        {
            /*_optionsDisplay.Display(TEST, OnOptionSelected);*/
        }
    }
}