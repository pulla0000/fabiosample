﻿using System;
using System.Collections.Generic;
using System.Linq;
using MagicianGames.Core.SavingSystem;
using UnityEngine;

namespace MagicianGames.AdventureGame.TitleScreen.OptionsScreen
{
    public class ResolutionOptionField : DropdownOption
    {
        private List<Resolution> _resolutions;
        private Resolution _initialResolution, _currentResolution;
       
        [Zenject.Inject]
        private ISettingsData _settings;

        public override bool HasChange => !_initialResolution.IsSameResolutionAs(_currentResolution);
        
        public override event Action OnValueChanged;
   
        public override void Setup()
        {
            if (_resolutions == null)
            {
                _resolutions = new List<Resolution>();
                
                foreach (var r in Screen.resolutions)
                {
                    if (_resolutions.ContainsResolution(r))
                        continue;

                    _resolutions.Add(r);
                }
                    
                base.Setup();
                PopulateDropdown();
            }

            _initialResolution =
                _resolutions.FirstOrDefault(res => res.IsSameResolutionAs(_settings.Settings.Resolution));
            
            _currentResolution = _initialResolution;
            _dropdown.SetValueWithoutNotify(_resolutions.IndexOf(_currentResolution));
        }

        public override void ApplyChange()
        {
            _initialResolution = _currentResolution;
            _settings.Settings.SetResolution(_currentResolution);
        }

        protected override void OnSelect()
        {
           // _optionsDisplay.Display(GetResolutionsTexts(), OnOptionSelected);
        }

        protected override List<string> GetOptions()
        {
            var values = new List<string>();

            foreach (var r in _resolutions)
            {
                values.Add(r.width + "x" + r.height);
            }

            return values;
        }

        protected override void OnValueSelected(int result)
        {
            var res = _resolutions[result];

            if (_currentResolution.IsSameResolutionAs(res))
            {
                return;
            }

            _currentResolution = res;
            OnValueChanged?.Invoke();
        }
    }
}

public static class ResolutionExtention
{
    public static bool IsSameResolutionAs(this Resolution res, Resolution other)
    {
        return res.width == other.width && res.height == other.height;
    }
    
    public static bool IsSameResolutionAs(this Resolution res, SettingsData.ResolutionData other)
    {
        return res.width == other.Width && res.height == other.Height;
    }
    
    public static bool ContainsResolution(this List<Resolution> list,  Resolution other)
    {
       return list.Any(res => res.IsSameResolutionAs(other));
    }
}