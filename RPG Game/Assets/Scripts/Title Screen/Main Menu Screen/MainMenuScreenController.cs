﻿using System;
using System.Collections;
using System.Collections.Generic;
using Core.InputSystem;
using Core.InputSystem.RumblePatterns;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

namespace MagicianGames.AdventureGame.TitleScreen.MainMenu
{
    public class MainMenuScreenController : MonoBehaviour, IScreen
    {
        [SerializeField]
        private GameObject _window;

        [SerializeField]
        private GameObject _defaultSelection;

        /*[SerializeField]
        private TextMeshProUGUI _devicesText;*/
        
        private GameObject _lastSelection;
        private IScreenNavigation _screenNavigation;
        
        public bool IsOpen => _window.activeSelf;

        private void Start()
        {
            _screenNavigation.OpenScreen(this);
        }

        [Zenject.Inject]
        private void Register(IScreenNavigation navigation)
        {
            _screenNavigation = navigation;
            _screenNavigation.RegisterScreen("MAIN", this);
        }

        /*private IInputSystem _inputSystem;
        
        [Zenject.Inject]
        private void RegisterInput(IInputSystem inputSystem)
        {
            _inputSystem = inputSystem;
            SetDevices(inputSystem.GetPairedDevices());
        }*/
        
        public void Open()
        {
            if (IsOpen)
            {
                return;
            }

            if (_lastSelection == null)
            {
                _lastSelection = _defaultSelection;
            }
            
            _window.SetActive(true);
            EventSystem.current.SetSelectedGameObject(_lastSelection);
        }

        public void Close()
        {
            _window.SetActive(false);
        }

        #region Button Callback

        public void OnClickPlay(GameObject sender)
        {
            _lastSelection = sender;
            _screenNavigation.OpenScreen(ScreenName.PLAY_SCREEN);
        }

        public void OnClickOptions(GameObject sender)
        {
            _lastSelection = sender;
            _screenNavigation.OpenScreen(ScreenName.OPTIONS_SCREEN);
        }

        public void OnClickCredits(GameObject sender)
        {
            _lastSelection = sender;
            _screenNavigation.OpenScreen(ScreenName.CREDITS_SCREEN);
        }

        public void OnClickQuit()
        {
            Application.Quit();
        }

        #endregion
        
        /*private void OnEnable()
        {
            NotificationsManager.AddListener<ControllerInputChangedNotification>(OnInputDeviceChange);
        }
        
        private void OnDisable()
        {
            NotificationsManager.RemoveListener<ControllerInputChangedNotification>(OnInputDeviceChange);
        }
        
        private void OnInputDeviceChange(ControllerInputChangedNotification notification)
        {
            SetDevices(notification.PairedDevices);
        }

        private void SetDevices(IReadOnlyList<string> devices)
        {
            _inputSystem.GetGamepad()?.StartRumble(new SimpleRumble(.5f, 2));
            
            if (devices == null)
            {
                return;
            }
            
            _devicesText.text = string.Empty;
            foreach (var device in devices)
            {
                _devicesText.text += device+"\n";
            }
        }*/
    }
}