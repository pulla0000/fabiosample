﻿using MagicianGames.Core.SavingSystem;
using System.Collections;
using System.Collections.Generic;
using Core.Ui;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;

namespace MagicianGames.AdventureGame.TitleScreen.PlayScreen
{
    public class PlayScreenController : MonoBehaviour, IScreen, ICancelHandler
    {
        [SerializeField]
        private GameObject _window;

        [SerializeField]
        private Transform _slotsParent;

        [SerializeField]
        private GameObject _defaultSlot;

        /*[SerializeField]
        private InputActionReference _deleteSlotReference;*/
        
        [Zenject.Inject]
        private IGameData _gameData;

        [Zenject.Inject]
        private IScenesLoader _scenesLoader;

        [Zenject.Inject]
        private IConfirmationPopup _confirmationPopup;
        
        private IScreenNavigation _screenNavigation;
        private SlotController _slotToDelete;
        private InputActionHandler _inputActionHandler;
        
        public bool IsOpen => _window.activeSelf;

        // Start is called before the first frame update
        void Start()
        {
            _inputActionHandler = GetComponent<InputActionHandler>();
            LoadSlots();
            Close();
        }

        private async void LoadSlots()
        {
            for (int i = 0; i < _slotsParent.childCount; i++)
            {
                var slotController = _slotsParent.GetChild(i).GetComponent<SlotController>();
                var slot = await SlotLoader.LoadSlotAsync(i);

                slotController.Setup(slot);
                slotController.OnSlotClicked += OnSlotClicked;
            }
        }

        [Zenject.Inject]
        private void Register(IScreenNavigation navigation)
        {
            _screenNavigation = navigation;
            _screenNavigation.RegisterScreen(ScreenName.PLAY_SCREEN, this);
        }

        private void EnableInput()
        {
            /*_deleteSlotReference.action.performed += DeleteSelectedSlot;
            _deleteSlotReference.action.Enable();*/
            _inputActionHandler.EnableInput();
        }

        private void DisableInput()
        {
            /*_deleteSlotReference.action.performed -= DeleteSelectedSlot;
            _deleteSlotReference.action.Disable();*/
            _inputActionHandler.DisableInput();
        }
        
        public void Open()
        {
            if (IsOpen)
                return;

            _window.SetActive(true);
            EventSystem.current.SetSelectedGameObject(_defaultSlot);
            EnableInput();
        }

        public void Close()
        {
            DisableInput();
            _window.SetActive(false);
        }

        private void OnSlotClicked(SlotData slot)
        {
            if (_slotToDelete != null)
            {
                return;
            }
            
            var sceneToLoad = "MainCity";

            if (!string.IsNullOrEmpty(slot.GameData.CurrentScene))
            {
                sceneToLoad = slot.GameData.CurrentScene;
            }
            
            _gameData.SetSlot(slot);
            _scenesLoader.LoadScene(sceneToLoad, showLoading: false);
            Close();
        }

        public void OnCancel(BaseEventData eventData)
        {
            if (_slotToDelete != null)
            {
                return;
            }
            
            _screenNavigation.GoToPreviewScreen();
        }

        [UsedImplicitly]
        public void DeleteSelectedSlot()
        {
            DisableInput();
            _slotToDelete = EventSystem.current.currentSelectedGameObject.GetComponent<SlotController>();

            if (_slotToDelete == null)
            {
                Debug.LogError(EventSystem.current.currentSelectedGameObject.name+
                               " doesn't has a SlotController component", EventSystem.current.currentSelectedGameObject);
                return;
            }
            
            _confirmationPopup.Open("Are you sure you want to delete this slot?", OnDeleteResult, 
                "Delete","Cancel");
        }

        private void OnDeleteResult(int index)
        {
            switch (index)
            {
                case 0:
                    _slotToDelete.DeleteSlot();
                    break;
            }

            StartCoroutine(WaitToEnableInput());
        }

        private IEnumerator WaitToEnableInput()
        {
            yield return 0;
            
            _slotToDelete = null;
            EnableInput();
        }
    }
}