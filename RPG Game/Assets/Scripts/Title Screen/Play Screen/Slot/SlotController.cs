﻿using MagicianGames.Core.SavingSystem;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MagicianGames.AdventureGame.TitleScreen.PlayScreen
{
    public class SlotController : MonoBehaviour
    {
        private SlotView _view;
        private SlotData _slotData;
        public event System.Action<SlotData> OnSlotClicked;

        private void SetupView()
        {
            if (_view == null)
                _view = GetComponent<SlotView>();
        }

        public void Setup(SlotData slot)
        {
            SetupView();
            _slotData = slot;

            _view.Setup(null, slot.GameData.CurrentScene, GetTime(slot.GameTime));
        }

        private string GetTime(float gameTime)
        {
            var time = System.TimeSpan.FromSeconds(gameTime);
            return time.ToString("hh':'mm':'ss");
        }

        public void DeleteSlot()
        {
            _slotData.Delete();
            SlotLoader.SaveSlot(_slotData);
            Setup(_slotData);
        }
        
        public void OnSlotClick()
        {
            OnSlotClicked?.Invoke(_slotData);
        }
    }
}