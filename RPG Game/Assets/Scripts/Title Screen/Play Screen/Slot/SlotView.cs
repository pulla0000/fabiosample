﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

namespace MagicianGames.AdventureGame.TitleScreen.PlayScreen
{
    public class SlotView : MonoBehaviour
    {
        [SerializeField]
        private Image _areaImage;

        [SerializeField]
        private TextMeshProUGUI _areaNameText, _playTimeText;

        public void Setup(Sprite areaSprite, string areaName, string playTime)
        {
            _areaImage.overrideSprite = areaSprite;
            _areaNameText.text = areaName;
            _playTimeText.text = playTime;
        }
    }
}